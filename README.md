# java-nlp

## Location

Web: [http://138.197.141.38:3000](http://138.197.141.38:3000)

BackEnd: [http://138.197.141.38:4567](http://138.197.141.38:4567)

## Installation

### Git

Install git  
`yum install git -y`

Add repository for node 8.x   
`curl -sL https://rpm.nodesource.com/setup_8.x | bash -`
`# https://github.com/nodesource/distributions`

### Node

Install nodejs  
`yum install nodejs -y`

Check the version  
`node --version # should be 8 (yarn requiers a version <= 9)`

Install yarn
`npm install yarn -g`

Check the version  
`yarn --version`


### Java

JRE  
`yum install java-1.8.0-openjdk -y`

JDK  
`yum install java-1.8.0-openjdk-devel -y`


### Maven

Download Maven  
`curl -OL https://archive.apache.org/dist/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz`

Unarchive it  
`tar -xzvf apache-maven-3.5.2-bin.tar.gz`

Place it under `/opt`    
`mv apache-maven-3.5.2 /opt/`

Add path variables to `~/.bash_profile`  
`JAVA_HOME=/usr/java/default/`  
`PATH=$PATH:/opt/apache-maven-3.5.2/bin`

Check the version  
`mvn --version`

## Launch 

### backend component

Move to the root folder  
`cd nlp_agent`

Compile  
`mvn compile`

Run
`mvn exec:java -Dexec.mainClass="Main"` OR 

`nohup mvn exec:java -Dexec.mainClass="Main" &` # run in background

Make a bin directory at the root
`mkdir bin`

### frontend component

Move to the root folder  
`cd web`

Change the base url in store.js
```javascript
class Store {
  constructor() {
    this.instance = axios.create({
      baseURL: 'http://138.197.141.38:4567' // use your server IP
    });
  }
```

Install node packages  
`yarn install`

Run  
`yarn run start` OR 

`nohup yarn run start &` # run in background 

## Populate wiki database

Check if the server is running
`curl http://localhost/ping` should return "Hello world!"

Prepare wiki data dump
1. `curl http://localhost/downloadwikidb`
2. `curl http://localhost/unzipwikidb`
3. `curl http://localhost/populatewikidb` -> wait for ~ 3 hours

Now, open a browser and peform "Build Knowledge Base" task

## Run tests order

1. WebScrapingTests (out: scraper)  
2. NaturalLanguageProcessingTests (out: lda)  
3. NamedEntityRecognitionTests (out: ner)  
4. TopicIdentificationProcessorTests (out: impliedTopics)  
5. SimplifyCourseItemsTests (out: impliedTopics.simple)  
6. KnowledgeGraphBuilderTests (out: knowledgeGraph)  
7. NaiveBayesClassifierProcessorTests (out: bayesGraph)


