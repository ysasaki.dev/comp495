import knowledgeGraphBuilder.KnowledgeBaseBuilder;
import knowledgeGraphBuilder.KnowledgeGraphBuilder;
import knowledgeGraphBuilder.KnowledgeGraphBuilderConfig;
import org.junit.Test;

import java.io.IOException;

public class KnowledgeGraphBuilderTests extends TestBase {

  private KnowledgeGraphBuilder createModelingInstance(String target) throws IOException {
    String inFileName = PROJECT_BASE_PATH + "out/impliedTopics/" + target + ".json";
    String outFileName = inFileName
        .replace("target/classes", "src/main/resources")
        .replace("impliedTopics", "knowledgeGraph");
    KnowledgeGraphBuilderConfig config = new KnowledgeGraphBuilderConfig(
        inFileName,
        outFileName
    );
    KnowledgeBaseBuilder knowledgeBaseBuilder = new KnowledgeBaseBuilder();
    return new KnowledgeGraphBuilder(config, knowledgeBaseBuilder);
  }

  @Test
  public void AthabascaUniversity_Tests() throws Exception {
    KnowledgeGraphBuilder modeling = createModelingInstance("AthabascaUniversity");
    modeling.produceGraphFromExplicitPrerequisitesRelationship();
  }

  @Test
  public void PrincetonUniversity_Tests() throws Exception {
    KnowledgeGraphBuilder modeling = createModelingInstance("PrincetonUniversity");
    modeling.produceGraphFromExplicitPrerequisitesRelationship();
  }

  @Test
  public void UniversityOfTexasAtAustin_Tests() throws Exception {
    KnowledgeGraphBuilder modeling = createModelingInstance("UniversityOfTexasAtAustin");
    modeling.produceGraphFromExplicitPrerequisitesRelationship();
  }

  @Test
  public void UniversityOfWashington_Tests() throws Exception {
    KnowledgeGraphBuilder modeling = createModelingInstance("UniversityOfWashington");
    modeling.produceGraphFromExplicitPrerequisitesRelationship();
  }

  @Test
  public void All() throws Exception {
    AthabascaUniversity_Tests();
    PrincetonUniversity_Tests();
    UniversityOfTexasAtAustin_Tests();
    UniversityOfWashington_Tests();
  }
}
