import common.graph.Graph;
import common.graph.Relationship;
import naiveBayesClassifier.NaiveBayesClassifier;
import naiveBayesClassifier.NaiveBayesClassifierConfig;
import naiveBayesClassifier.NaiveBayesClassifierProcessor;
import naiveBayesClassifier.NaiveBayesClassifierProcessorConfig;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NaiveBayesClassifierProcessorTests extends TestBase {

  private NaiveBayesClassifierProcessor createModelingInstance(String target) throws IOException {
    String inFileName = PROJECT_BASE_PATH + "out/knowledgeGraph/" + target + ".json";
    String outFileName = inFileName
        .replace("target/classes", "src/main/resources")
        .replace("knowledgeGraph", "bayesGraph");
    NaiveBayesClassifierProcessorConfig config = new NaiveBayesClassifierProcessorConfig(
        inFileName,
        outFileName
    );
    NaiveBayesClassifierConfig naiveBayesClassifierConfig = new NaiveBayesClassifierConfig();
    NaiveBayesClassifier naiveBayesClassifier = new NaiveBayesClassifier(naiveBayesClassifierConfig);
    return new NaiveBayesClassifierProcessor(config, naiveBayesClassifier);
  }

  @Test
  public void buildTopicGraph() throws Exception {
    String[] trainingUniversities = {
        "PrincetonUniversity",
        "UniversityOfTexasAtAustin",
        "UniversityOfWashington",
    };
    List<Graph<Relationship>> trainingGraphs = new ArrayList<>();
    for (String university : trainingUniversities) {
      NaiveBayesClassifierProcessor modeling = createModelingInstance(university);
      trainingGraphs.add(modeling.readGraph());
    }
    String testUniversity = "AthabascaUniversity";
    NaiveBayesClassifierProcessor modeling = createModelingInstance(testUniversity);
    Graph<Relationship> testGraph = modeling.readGraph();

    modeling.createClassifierGraph(trainingGraphs, testGraph);
  }
}
