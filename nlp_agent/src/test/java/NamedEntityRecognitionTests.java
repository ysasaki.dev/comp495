import common.util.ResourceManager;
import common.util.StopwordsProvider;
import common.util.Tokenizer;
import namedEntityRecognition.NamedEntityRecognitionProcessor;
import namedEntityRecognition.NamedEntityRecognitionProcessorConfig;
import namedEntityRecognition.NamedEntityRecognizer;
import namedEntityRecognition.NamedEntityRecognizerConfig;
import namedEntityRecognition.wikiDb.WikiDbRepositoryConfig;
import org.junit.Test;

import java.io.IOException;

public class NamedEntityRecognitionTests extends TestBase {

  private NamedEntityRecognitionProcessor createModelingInstance(String target) throws IOException {
    String inFileName = PROJECT_BASE_PATH + "out/lda/" + target + ".json";
    String outFileName = inFileName
        .replace("target/classes", "src/main/resources")
        .replace("lda", "ner");
    String[] stopwordsPaths = {
        ResourceManager.get("stoplists/en.txt").toString(),
        ResourceManager.get("stoplists/compsci.txt").toString()
    };
    String dbFilePath = ResourceManager.get("bin/leveldb").toString();

    NamedEntityRecognitionProcessorConfig config = new NamedEntityRecognitionProcessorConfig(
        inFileName,
        outFileName
    );
    WikiDbRepositoryConfig wikiDbRepositoryConfig = new WikiDbRepositoryConfig(dbFilePath);
    NamedEntityRecognizerConfig namedEntityRecognizerConfig = new NamedEntityRecognizerConfig(wikiDbRepositoryConfig);
    NamedEntityRecognizer namedEntityRecognizer = new NamedEntityRecognizer(namedEntityRecognizerConfig);
    Tokenizer tokenizer = new Tokenizer();
    StopwordsProvider stopwordsProvider = new StopwordsProvider(stopwordsPaths);
    return new NamedEntityRecognitionProcessor(config, namedEntityRecognizer, tokenizer, stopwordsProvider);
  }

  @Test
  public void AthabascaUniversity_Tests() throws Exception {
    NamedEntityRecognitionProcessor modeling = createModelingInstance("AthabascaUniversity");
    modeling.processNERIdentification();
  }

  @Test
  public void PrincetonUniversity_Tests() throws Exception {
    NamedEntityRecognitionProcessor modeling = createModelingInstance("PrincetonUniversity");
    modeling.processNERIdentification();
  }

  @Test
  public void UniversityOfTexasAtAustin_Tests() throws Exception {
    NamedEntityRecognitionProcessor modeling = createModelingInstance("UniversityOfTexasAtAustin");
    modeling.processNERIdentification();
  }

  @Test
  public void UniversityOfWashington_Tests() throws Exception {
    NamedEntityRecognitionProcessor modeling = createModelingInstance("UniversityOfWashington");
    modeling.processNERIdentification();
  }

  @Test
  public void All() throws Exception {
    AthabascaUniversity_Tests();
    PrincetonUniversity_Tests();
    UniversityOfTexasAtAustin_Tests();
    UniversityOfWashington_Tests();
  }
}
