import org.junit.Test;
import simplification.SimplificationProcessor;
import simplification.SimplificationProcessorConfig;

import java.io.IOException;

public class SimplifyCourseItemsTests extends TestBase {

  private SimplificationProcessor createModelingInstance(String target) throws IOException {
    String inFileName = PROJECT_BASE_PATH + "out/impliedTopics/" + target + ".json";
    String outFileName = inFileName
        .replace("target/classes", "src/main/resources")
        .replace("impliedTopics", "impliedTopics.simple");
    SimplificationProcessorConfig config = new SimplificationProcessorConfig(
        inFileName,
        outFileName
    );
    return new SimplificationProcessor(config);
  }

  @Test
  public void AthabascaUniversity_Tests() throws Exception {
    SimplificationProcessor modeling = createModelingInstance("AthabascaUniversity");
    modeling.produceSimpleCourseModels();
  }

  @Test
  public void PrincetonUniversity_Tests() throws Exception {
    SimplificationProcessor modeling = createModelingInstance("PrincetonUniversity");
    modeling.produceSimpleCourseModels();
  }

  @Test
  public void UniversityOfTexasAtAustin_Tests() throws Exception {
    SimplificationProcessor modeling = createModelingInstance("UniversityOfTexasAtAustin");
    modeling.produceSimpleCourseModels();
  }

  @Test
  public void UniversityOfWashington_Tests() throws Exception {
    SimplificationProcessor modeling = createModelingInstance("UniversityOfWashington");
    modeling.produceSimpleCourseModels();
  }

  @Test
  public void All() throws Exception {
    AthabascaUniversity_Tests();
    PrincetonUniversity_Tests();
    UniversityOfTexasAtAustin_Tests();
    UniversityOfWashington_Tests();
  }

}
