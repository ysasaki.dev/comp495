import common.util.ResourceManager;
import common.util.StopwordsProvider;
import org.junit.Test;
import topicIdentification.TopicIdentificationProcessor;
import topicIdentification.TopicIdentificationProcessorConfig;

import java.io.IOException;

public class TopicIndentificationProcessorTests extends TestBase {

  private TopicIdentificationProcessor createModelingInstance(String target) throws IOException {
    String inFileName = PROJECT_BASE_PATH + "out/ner/" + target + ".json";
    String outFileName = inFileName
        .replace("target/classes", "src/main/resources")
        .replace("ner", "impliedTopics");
    String[] stopwordsPaths = {
        ResourceManager.get("stoplists/en.txt").toString(),
        ResourceManager.get("stoplists/compsci.txt").toString()
    };
    TopicIdentificationProcessorConfig config = new TopicIdentificationProcessorConfig(
        inFileName,
        outFileName
    );
    StopwordsProvider stopwordsProvider = new StopwordsProvider(stopwordsPaths);
    return new TopicIdentificationProcessor(config, stopwordsProvider);
  }

  @Test
  public void AthabascaUniversity_Tests() throws Exception {
    TopicIdentificationProcessor modeling = createModelingInstance("AthabascaUniversity");
    modeling.processTopicIdentification();
  }

  @Test
  public void PrincetonUniversity_Tests() throws Exception {
    TopicIdentificationProcessor modeling = createModelingInstance("PrincetonUniversity");
    modeling.processTopicIdentification();
  }

  @Test
  public void UniversityOfTexasAtAustin_Tests() throws Exception {
    TopicIdentificationProcessor modeling = createModelingInstance("UniversityOfTexasAtAustin");
    modeling.processTopicIdentification();
  }

  @Test
  public void UniversityOfWashington_Tests() throws Exception {
    TopicIdentificationProcessor modeling = createModelingInstance("UniversityOfWashington");
    modeling.processTopicIdentification();
  }

  @Test
  public void All() throws Exception {
    AthabascaUniversity_Tests();
    PrincetonUniversity_Tests();
    UniversityOfTexasAtAustin_Tests();
    UniversityOfWashington_Tests();
  }
}
