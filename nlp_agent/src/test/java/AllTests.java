import checkerModule.AthabascaUniversityGraphBuilder;
import checkerModule.AthabascaUniversityGraphBuilderResult;
import common.util.Repository;
import org.junit.Test;

public class AllTests {

  @Test
  public void NER_to_KnowledgeGraphBuilder() throws Exception {

    new NaturalLanguageProcessingTests().All();
    new NamedEntityRecognitionTests().All();
    new TopicIndentificationProcessorTests().All();
    new SimplifyCourseItemsTests().All();
    new KnowledgeGraphBuilderTests().All();
    new NaiveBayesClassifierProcessorTests().buildTopicGraph();

  }

  @Test
  public void AthabascaUniversityGraphBuilder() throws Exception {
    String input = "http://www.athabascau.ca/course/course-listings.php?/undergraduate/all/all";
    String data = "/Users/ysasaki/workspace/AU-COMP-495/nlp_agent/data/";
    String leveldb = "/Users/ysasaki/workspace/AU-COMP-495/nlp_agent/bin/leveldb/";
    Repository repository = new Repository(leveldb, data);
    AthabascaUniversityGraphBuilder builder = new AthabascaUniversityGraphBuilder(repository);
    AthabascaUniversityGraphBuilderResult result = builder.createGraph(input, "comp");
  }
}
