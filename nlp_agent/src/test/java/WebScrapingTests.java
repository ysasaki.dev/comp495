import common.model.CourseItem;
import org.junit.Test;
import common.util.JsonResourceManager;
import webScraping.Scraper;
import webScraping.impl.AthabascaUniversityScraper;
import webScraping.impl.PrincetonUniversityScraper;
import webScraping.impl.UniversityOfTexasAtAustineScraper;
import webScraping.impl.UniversityOfWashingtonScraper;

import java.util.List;

public class WebScrapingTests extends TestBase {

  @Test
  public void AthabascaUniversityScraperTest(){
    String input = "http://www.athabascau.ca/course/course-listings.php?/undergraduate/all/all";
    Scraper scraper = new AthabascaUniversityScraper(input, "comp");
    scraper.setPrintStream(System.out);
    scraper.scrape();
    List<CourseItem> courses = scraper.getCourses();
    saveCourseItemsAsJson(courses, "AthabascaUniversity.out.json");
  }

  @Test
  public void UniversityOfTexasAtAustinScraperTest(){
    Scraper scraper = new UniversityOfTexasAtAustineScraper();
    scraper.setPrintStream(System.out);
    scraper.scrape();
    List<CourseItem> courses = scraper.getCourses();
    saveCourseItemsAsJson(courses, "UniversityOfTexasAtAustin.out.json");
  }

  @Test
  public void UniversityOfWashingtonScraperTest(){
    Scraper scraper = new UniversityOfWashingtonScraper();
    scraper.setPrintStream(System.out);
    scraper.scrape();
    List<CourseItem> courses = scraper.getCourses();
    saveCourseItemsAsJson(courses, "UniversityOfWashington.out.json");
  }

  @Test
  public void PrincetonUniversityScraperTest(){
    Scraper scraper = new PrincetonUniversityScraper();
    scraper.setPrintStream(System.out);
    scraper.scrape();
    List<CourseItem> courses = scraper.getCourses();
    saveCourseItemsAsJson(courses, "PrincetonUniversity.out.json");
  }

  private void saveCourseItemsAsJson(List<CourseItem> courses, String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, CourseItem.class, null).saveAsJson(courses);
  }
}
