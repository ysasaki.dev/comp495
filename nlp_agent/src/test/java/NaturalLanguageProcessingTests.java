import common.util.ResourceManager;
import common.util.StopwordsProvider;
import naturalLanguageProcessing.ModelerFactory;
import naturalLanguageProcessing.NLPProcessor;
import naturalLanguageProcessing.NLPProcessorConfig;
import org.junit.Test;

import java.io.IOException;

public class NaturalLanguageProcessingTests extends TestBase {

  private NLPProcessor createModelingInstance(String target) throws IOException {
    String inFileName = PROJECT_BASE_PATH + "out/scraper/" + target + ".json";
    String outFileName = inFileName
        .replace("target/classes", "src/main/resources")
        .replace("scraper", "lda");
    String ldaFileName = inFileName
        .replace("target/classes", "src/main/resources")
        .replace("scraper", "lda")
        .replace("json", "txt");
    String[] stopwordsPaths = {
        ResourceManager.get("stoplists/en.txt").toString(),
        ResourceManager.get("stoplists/compsci.txt").toString()
    };
    NLPProcessorConfig config = new NLPProcessorConfig(
        inFileName,
        outFileName,
        ldaFileName
    );
    ModelerFactory modelerFactory = new ModelerFactory();
    StopwordsProvider stopwordsProvider = new StopwordsProvider(stopwordsPaths);
    return new NLPProcessor(config, modelerFactory, stopwordsProvider);
  }

  @Test
  public void AthabascaUniversity_Tests() throws Exception {
    NLPProcessor modeling = createModelingInstance("AthabascaUniversity");
    modeling.processLDAModeling();
  }

  @Test
  public void PrincetonUniversity_Tests() throws Exception {
    NLPProcessor modeling = createModelingInstance("PrincetonUniversity");
    modeling.processLDAModeling();
  }

  @Test
  public void UniversityOfTexasAtAustin_Tests() throws Exception {
    NLPProcessor modeling = createModelingInstance("UniversityOfTexasAtAustin");
    modeling.processLDAModeling();
  }

  @Test
  public void UniversityOfWashington_Tests() throws Exception {
    NLPProcessor modeling = createModelingInstance("UniversityOfWashington");
    modeling.processLDAModeling();
  }

  @Test
  public void All() throws Exception {
    AthabascaUniversity_Tests();
    PrincetonUniversity_Tests();
    UniversityOfTexasAtAustin_Tests();
    UniversityOfWashington_Tests();
  }
}
