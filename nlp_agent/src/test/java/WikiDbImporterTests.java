import org.testng.annotations.Test;
import common.util.ResourceManager;
import namedEntityRecognition.wikiDb.WikiDbImporter;
import namedEntityRecognition.wikiDb.WikiDbImporterConfig;
import namedEntityRecognition.wikiDb.WikiDbRepositoryConfig;

import java.io.IOException;

public class WikiDbImporterTests extends TestBase {

  private WikiDbImporter create() throws IOException {
    String dbFilePath = "bin/leveldb";
    String stopwordsListFilePath = ResourceManager.get("stoplists/en.txt").toString();
    String wikiTitleFilePath = ResourceManager.get("bin/enwiki-latest-all-titles-in-ns0").toString();
    WikiDbRepositoryConfig wikiDbRepositoryConfig = new WikiDbRepositoryConfig(dbFilePath);
    WikiDbImporterConfig config = new WikiDbImporterConfig(stopwordsListFilePath, wikiTitleFilePath, wikiDbRepositoryConfig);
    WikiDbImporter wikiDbImporter = new WikiDbImporter(config);
    return wikiDbImporter;
  }

  @Test
  public void popualte() throws IOException {
    WikiDbImporter wikiDbImporter = create();
    wikiDbImporter.populateDb();
  }

}
