/**
 * NaiveBayesClassifierProcessor.java
 *
 * Author: Yu Sasaki
 *
 * This class performs Naive Bayes Classification for the given topic graph.
 * It internally uses NaiveBayesClassifier class
 *
 * */
package naiveBayesClassifier;

import common.graph.Graph;
import common.graph.Relationship;
import common.graph.impl.CourseItemNode;
import common.util.FileUtil;
import common.util.JsonResourceManager;

import java.util.List;

public class NaiveBayesClassifierProcessor {

  private NaiveBayesClassifierProcessorConfig config;
  private NaiveBayesClassifier naiveBayesClassifier;

  public NaiveBayesClassifierProcessor(
      NaiveBayesClassifierProcessorConfig config,
      NaiveBayesClassifier naiveBayesClassifier
  ) {
    this.config = config;
    this.naiveBayesClassifier = naiveBayesClassifier;
  }

  public Graph<Relationship> readGraph() throws Exception {
    return FileUtil.readGraphFromJson(config.getInFileName(), CourseItemNode[].class);
  }

  public void createClassifierGraph(List<Graph<Relationship>> trainigGraph, Graph<Relationship> testGraph) throws Exception {
    Graph<Relationship> graph = getClassifierGraph(trainigGraph, testGraph);
    this.saveGraphAsJson(graph, config.getOutFileName());
  }

  public Graph<Relationship> getClassifierGraph(List<Graph<Relationship>> trainigGraph, Graph<Relationship> testGraph) {
    Graph<Relationship> graph = this.naiveBayesClassifier.buildTopicGraph(trainigGraph, testGraph);
    return graph;
  }

  private void saveGraphAsJson(Graph<Relationship> graph, final String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<Graph<Relationship>> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, Graph.class, CourseItemNode[].class).saveAsJson(graph);
  }
}
