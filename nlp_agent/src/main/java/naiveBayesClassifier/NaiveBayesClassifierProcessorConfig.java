/**
 * NaiveBayesClassifierProcessorConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for NaiveBayesClassifierProcessor.
 *
 * */
package naiveBayesClassifier;

public class NaiveBayesClassifierProcessorConfig {

  private String inFileName;
  private String outFileName;

  public NaiveBayesClassifierProcessorConfig(
      String inFileName,
      String outFileName){

    this.inFileName = inFileName;
    this.outFileName = outFileName;
  }

  public String getInFileName() {
    return inFileName;
  }

  public String getOutFileName() {
    return outFileName;
  }

}
