/**
 * NaiveBayesClassifier.java
 *
 * Author: Yu Sasaki
 *
 * This class performs Naive Bayes Classification for the given topic graph.
 *
 *
 * */
package naiveBayesClassifier;

import java.util.*;

import de.daslaboratorium.machinelearning.classifier.Classification;
import de.daslaboratorium.machinelearning.classifier.Classifier;
import de.daslaboratorium.machinelearning.classifier.bayes.BayesClassifier;
import common.graph.impl.CourseItemNode;
import common.graph.impl.TopicNode;
import common.graph.Edge;
import common.graph.Graph;
import common.graph.INode;
import common.graph.Relationship;

import static java.util.Comparator.*;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class NaiveBayesClassifier {

  private NaiveBayesClassifierConfig config;

  public NaiveBayesClassifier(NaiveBayesClassifierConfig config) {
    this.config = config;
  }

  public Classifier<String, String> trainClassifier(List<Graph<Relationship>> trainingGraphs) {
    Classifier<String, String> bayes = new BayesClassifier<>();
    bayes.setMemoryCapacity(10000);
    trainingGraphs.forEach(g -> g.getNodes().forEach(sourceNode -> {
      CourseItemNode source = (CourseItemNode) sourceNode;
      List<INode> targetNodes = g.getAllTargetNodes(sourceNode);
      targetNodes.forEach(targetNode -> {
        CourseItemNode target = (CourseItemNode) targetNode;
        target.getImpliedTopics().forEach(category -> {
          bayes.learn(category, source.getImpliedTopics());
        });
      });
    }));
    return bayes;
  }

  public Set<String> getTopicVocabuluary(Graph<Relationship> testGraph) {
    Set<String> result = testGraph.getNodes().stream().flatMap(n -> ((CourseItemNode) n).getImpliedTopics().stream()).sorted(String::compareToIgnoreCase).collect(toSet());
    return result;
  }

  public Graph<Relationship> buildTopicGraph(List<Graph<Relationship>> trainingGraphs, Graph<Relationship> testGraph) {

    Set<String> testVocabulary = getTopicVocabuluary(testGraph);

    Classifier<String, String> bayes = trainClassifier(trainingGraphs);

    final int top = 6;

    final Set<String> vocabulary = new HashSet<>();

    testVocabulary.forEach(topic -> {
      Collection<Classification<String, String>> dependentTopicDetails = ((BayesClassifier<String, String>) bayes).classifyDetailed(Arrays.asList(topic));
      Collection<Classification<String, String>> topNTopics = dependentTopicDetails.stream().sorted(comparing(Classification<String, String>::getProbability).reversed()).limit(top).collect(toList());
      topNTopics.forEach(d -> {
        vocabulary.add(d.getCategory());
      });
    });

    // merge two sets
    vocabulary.addAll(testVocabulary);
    Graph<Relationship> graph = new Graph<>();

    // build a knowledgeGraphBuilder
    vocabulary.forEach(word -> {
      TopicNode node = new TopicNode(word);
      graph.addNode(node);
    });

    graph.getNodes().forEach(from -> {
      Collection<Classification<String, String>> dependentTopicDetails = ((BayesClassifier<String, String>) bayes).classifyDetailed(Arrays.asList(from.getId()));
      Collection<Classification<String, String>> topNTopics = dependentTopicDetails.stream().sorted(comparing(Classification<String, String>::getProbability).reversed()).limit(top).collect(toList());
      topNTopics.forEach(toTopic -> {
        INode to = graph.getNodeById(toTopic.getCategory());
        if (to != null) {
          Edge<Relationship> edge = new Edge<>(from, to, Relationship.Explicit, null);
          graph.addEdge(edge, Relationship.Explicit);
        }
      });
    });

    // remove nodes who don't have children
    List<INode> toBeRemoved = new ArrayList<>();
    graph.getNodes().forEach(from -> {
      List<INode> sources = graph.getAllSourceNodes(from);
      if(sources.isEmpty()){
        toBeRemoved.add(from);
      }
    });
    toBeRemoved.forEach(graph::removeNode);

    return graph;
  }
}
