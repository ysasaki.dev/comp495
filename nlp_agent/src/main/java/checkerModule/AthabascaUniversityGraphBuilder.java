package checkerModule;

import common.graph.Graph;
import common.graph.Relationship;
import common.model.CourseItem;
import common.model.SimpleCourseItem;
import common.util.Repository;
import common.util.StopwordsProvider;
import common.util.Tokenizer;
import knowledgeGraphBuilder.KnowledgeBaseBuilder;
import knowledgeGraphBuilder.KnowledgeGraphBuilder;
import knowledgeGraphBuilder.KnowledgeGraphBuilderConfig;
import naiveBayesClassifier.NaiveBayesClassifier;
import naiveBayesClassifier.NaiveBayesClassifierConfig;
import naiveBayesClassifier.NaiveBayesClassifierProcessor;
import naiveBayesClassifier.NaiveBayesClassifierProcessorConfig;
import namedEntityRecognition.NamedEntityRecognitionProcessor;
import namedEntityRecognition.NamedEntityRecognitionProcessorConfig;
import namedEntityRecognition.NamedEntityRecognizer;
import namedEntityRecognition.NamedEntityRecognizerConfig;
import namedEntityRecognition.wikiDb.WikiDbRepositoryConfig;
import naturalLanguageProcessing.ModelerFactory;
import naturalLanguageProcessing.NLPProcessor;
import naturalLanguageProcessing.NLPProcessorConfig;
import simplification.SimplificationProcessor;
import simplification.SimplificationProcessorConfig;
import topicIdentification.TopicIdentificationProcessor;
import topicIdentification.TopicIdentificationProcessorConfig;
import webScraping.impl.AthabascaUniversityScraper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AthabascaUniversityGraphBuilder {

    private Repository repository;
    private String stoplistEnPath;
    private String stoplistCompsciPath;

    public AthabascaUniversityGraphBuilder(Repository repository){

        this.repository = repository;
        this.stoplistEnPath = this.repository.getDataFilePathString("stoplists/en.txt");
        this.stoplistCompsciPath = this.repository.getDataFilePathString("stoplists/compsci.txt");
    }

    private NLPProcessor createNlpModelingInstance() {
        String[] stopwordsPaths = {this.stoplistEnPath, this.stoplistCompsciPath};
        NLPProcessorConfig config = new NLPProcessorConfig(null, null, null);
        ModelerFactory modelerFactory = new ModelerFactory();
        StopwordsProvider stopwordsProvider = new StopwordsProvider(stopwordsPaths);
        return new NLPProcessor(config, modelerFactory, stopwordsProvider);
    }

    private NamedEntityRecognitionProcessor createNerModelingInstance() throws IOException {
        String[] stopwordsPaths = {this.stoplistEnPath, this.stoplistCompsciPath};
        String dbFilePath = this.repository.getLevelDbPathString();

        NamedEntityRecognitionProcessorConfig config = new NamedEntityRecognitionProcessorConfig(
                null,
                null
        );
        WikiDbRepositoryConfig wikiDbRepositoryConfig = new WikiDbRepositoryConfig(dbFilePath);
        NamedEntityRecognizerConfig namedEntityRecognizerConfig = new NamedEntityRecognizerConfig(wikiDbRepositoryConfig);
        NamedEntityRecognizer namedEntityRecognizer = new NamedEntityRecognizer(namedEntityRecognizerConfig);
        Tokenizer tokenizer = new Tokenizer();
        StopwordsProvider stopwordsProvider = new StopwordsProvider(stopwordsPaths);
        return new NamedEntityRecognitionProcessor(config, namedEntityRecognizer, tokenizer, stopwordsProvider);
    }

    private TopicIdentificationProcessor createTiModelingInstance() {
        String[] stopwordsPaths = {this.stoplistEnPath, this.stoplistCompsciPath};
        TopicIdentificationProcessorConfig config = new TopicIdentificationProcessorConfig(
                null,
                null
        );
        StopwordsProvider stopwordsProvider = new StopwordsProvider(stopwordsPaths);
        return new TopicIdentificationProcessor(config, stopwordsProvider);
    }

    private KnowledgeGraphBuilder createKgModelingInstance() {
        KnowledgeGraphBuilderConfig config = new KnowledgeGraphBuilderConfig(
                null,
                null
        );
        KnowledgeBaseBuilder knowledgeBaseBuilder = new KnowledgeBaseBuilder();
        return new KnowledgeGraphBuilder(config, knowledgeBaseBuilder);
    }

    private NaiveBayesClassifierProcessor createNbcModelingInstance(String target) {
        String inFileName = this.repository.getDataFilePathString("knowledgeGraph/" + target + ".json");
        NaiveBayesClassifierProcessorConfig config = new NaiveBayesClassifierProcessorConfig(
                inFileName,
                null
        );
        NaiveBayesClassifierConfig naiveBayesClassifierConfig = new NaiveBayesClassifierConfig();
        NaiveBayesClassifier naiveBayesClassifier = new NaiveBayesClassifier(naiveBayesClassifierConfig);
        return new NaiveBayesClassifierProcessor(config, naiveBayesClassifier);
    }

    private SimplificationProcessor createSmplModelingInstance() {
        SimplificationProcessorConfig config = new SimplificationProcessorConfig(
                null,
                null
        );
        return new SimplificationProcessor(config);
    }
    public AthabascaUniversityGraphBuilderResult createGraph(String input, String discipline) throws Exception {
        // scrape
        AthabascaUniversityScraper scraper = new AthabascaUniversityScraper(input, discipline);
        scraper.setPrintStream(System.out);
        scraper.scrape();
        List<CourseItem> courses = scraper.getCourses();

        // NLP Modeling
        NLPProcessor nlpProcessor = createNlpModelingInstance();
        courses = nlpProcessor.processLDAModeling(courses);

        // Named Entity Recognition
        NamedEntityRecognitionProcessor namedEntityRecognitionProcessor = createNerModelingInstance();
        courses = namedEntityRecognitionProcessor.processNERIdentification(courses);

        // Topic Identification
        TopicIdentificationProcessor topicIdentificationProcessor = createTiModelingInstance();
        courses = topicIdentificationProcessor.processTopicIdentification(courses);

        // Simple
        SimplificationProcessor simplificationProcessor = createSmplModelingInstance();
        List<SimpleCourseItem> simpleCourseItems = simplificationProcessor.produceSimpleCourseModels(courses);

        // Knowledge graph build
        KnowledgeGraphBuilder knowledgeGraphBuilder = createKgModelingInstance();
        Graph<Relationship> curriculumGraph = knowledgeGraphBuilder.produceGraphFromExplicitPrerequisitesRelationship(courses);

        // Naive Bayes Classifier
        String[] trainingUniversities = {
                "PrincetonUniversity",
                "UniversityOfTexasAtAustin",
                "UniversityOfWashington",
        };
        List<Graph<Relationship>> trainingGraphs = new ArrayList<>();
        for (String university : trainingUniversities) {
            NaiveBayesClassifierProcessor modeling = createNbcModelingInstance(university);
            trainingGraphs.add(modeling.readGraph());
        }
        String testUniversity = "AthabascaUniversity";
        NaiveBayesClassifierProcessor modeling = createNbcModelingInstance(testUniversity);
        Graph<Relationship> knowledgeGraph = modeling.getClassifierGraph(trainingGraphs, curriculumGraph);

        AthabascaUniversityGraphBuilderResult result = new AthabascaUniversityGraphBuilderResult(knowledgeGraph, simpleCourseItems, curriculumGraph);

        return result;
    }

}
