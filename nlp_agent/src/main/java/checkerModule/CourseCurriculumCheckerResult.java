/**
 * CheckCurriculumChckerResult.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a Result model for CourseCurriculumChecker.
 *
 * */
package checkerModule;

import common.graph.impl.CourseItemNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CourseCurriculumCheckerResult {

  public static class TopicCoveredDetails {
    public String topic;
    public boolean covered;
  }


  public static class CourseDetails {
    public CourseItemNode courseItem;
    public Set<String> coveredTopics = new HashSet<>();
    public CourseItemNode dependentFor;
  }

  private CourseItemNode selectedCourse;
  private List<CourseDetails> courseDetails = new ArrayList<>();
  private List<TopicCoveredDetails> topicCoveredDetails = new ArrayList<>();

  public CourseCurriculumCheckerResult(CourseItemNode selectedCourse) {
    this.selectedCourse = selectedCourse;
    this.courseDetails = new ArrayList<>();
  }

  public CourseItemNode getSelectedCourse() {
    return selectedCourse;
  }

  public List<CourseDetails> getCourseDetails() {
    return courseDetails;
  }

  public List<TopicCoveredDetails> getTopicCoveredDetails() {
    return topicCoveredDetails;
  }

}
