/**
 * CheckPrerequisitesChckerResult.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a Result model for CoursePrerequisitesChecker
 *
 * */
package checkerModule;

import java.util.*;
import common.graph.impl.CourseItemNode;

import java.util.ArrayList;

public class CoursePrerequisitesCheckerResult {

  public static class TopicDetails {
    public String topic;
    public boolean exists;
    public List<CourseItemNode> recommendedCourses;
  }

  private CourseItemNode selectedCourse;
  private List<TopicDetails> topicDetails = new ArrayList<>();

  public CoursePrerequisitesCheckerResult(CourseItemNode selectedCourse) {
    this.selectedCourse = selectedCourse;
    this.topicDetails = new ArrayList<>();
  }

  public CourseItemNode getSelectedCourse() {
    return selectedCourse;
  }

  public List<TopicDetails> getTopicDetails() {
    return topicDetails;
  }

}
