package checkerModule;

import common.graph.Graph;
import common.graph.Relationship;
import common.model.SimpleCourseItem;

import java.util.List;

public class AthabascaUniversityGraphBuilderResult {

    private Graph<Relationship> knowledgeGraph;
    private List<SimpleCourseItem> courses;
    private Graph<Relationship> curriculumGraph;

    public AthabascaUniversityGraphBuilderResult(
            Graph<Relationship> knowledgeGraph,
            List<SimpleCourseItem> courses,
            Graph<Relationship> curriculumGraph
    ) {
        this.knowledgeGraph = knowledgeGraph;
        this.courses = courses;
        this.curriculumGraph = curriculumGraph;
    }

    public Graph<Relationship> getKnowledgeGraph() {
        return knowledgeGraph;
    }

    public List<SimpleCourseItem> getCourses() {
        return courses;
    }

    public Graph<Relationship> getCurriculumGraph() {
        return curriculumGraph;
    }
}
