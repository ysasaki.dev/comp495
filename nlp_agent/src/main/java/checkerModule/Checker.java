/**
 * Checker.java
 *
 * Author: Yu Sasaki
 *
 * This class provides logic for performing checking prerequistes and checking curriculum using topic and course dependencies.
 *
 * */
package checkerModule;

import common.util.FileUtil;
import common.graph.impl.CourseItemNode;
import common.model.SimpleCourseItem;
import common.graph.impl.TopicNode;
import common.graph.Graph;
import common.graph.INode;
import common.graph.Relationship;
import common.util.JsonResourceManager;

import java.util.*;

public class Checker {

  private CoursePrerequisitesCheckerConfig config;

  public Checker(CoursePrerequisitesCheckerConfig config) {

    this.config = config;
  }

  private Set<String> getDependentTopicsForCourseFromTopicGraph(Graph<Relationship> topicGraph, CourseItemNode courseItem){
    // Expected topics - the topics that are supposed to be there for the course.
    Set<String> expectedTopics = new HashSet<>();
    topicGraph.getEdges().forEach(topicGraphEdge -> {
      courseItem.getImpliedTopics().forEach(courseTopic -> {
        if (topicGraphEdge.getSource().getId().equals(courseTopic)) {
          expectedTopics.add(topicGraphEdge.getTarget().getId());
        }
      });
    });
    return expectedTopics;
  }

  private Set<String> getDepedentTopicsForCourseFromCourseDependencies(Graph<Relationship> curriculumGraph, CourseItemNode courseItem){
    Set<String> actualTopics = new HashSet<>();
    curriculumGraph.getEdges().forEach(edge -> {
      if (edge.getSource().getId().equals(courseItem.getId())) {
        actualTopics.addAll (((CourseItemNode) edge.getTarget()).getImpliedTopics());
      }
    });
    return actualTopics;
  }

  private void bfs(INode currentCourse, Graph<Relationship> curriculumGraph, Set<String> topicsToBeCovered, CourseCurriculumCheckerResult result) {

    List<INode> dependentCourses = curriculumGraph.getAllTargetNodes(currentCourse);

    // for each dependent courses
    dependentCourses.forEach(dependentCourse -> {
      CourseCurriculumCheckerResult.CourseDetails details = new CourseCurriculumCheckerResult.CourseDetails();
      CourseItemNode courseItem = (CourseItemNode) dependentCourse;
      List<String> coveredTopicsbyCourse = courseItem.getImpliedTopics();

      // check if the expected topics are covered or not.
      Set<String> coveredTopics = new HashSet<>();
      topicsToBeCovered.forEach(topicToBeCovered -> {
        if (coveredTopicsbyCourse.contains(topicToBeCovered)) {
          coveredTopics.add(topicToBeCovered);
        }
      });

      details.dependentFor = (CourseItemNode) currentCourse;
      details.courseItem = courseItem;
      details.coveredTopics = coveredTopics;
      result.getCourseDetails().add(details);

      bfs(dependentCourse, curriculumGraph, topicsToBeCovered, result);
    });

  }

  private CourseCurriculumCheckerResult checkCurriculumProperlySetInternal(Graph<Relationship> topicGraph, Graph<Relationship> curriculumGraph, String courseTitle) throws Exception {

    // source course
    Optional<INode> sourceCourse = curriculumGraph.getNodes().stream().filter(a -> a.getId().equals(courseTitle)).findFirst();
    CourseItemNode courseItem = (CourseItemNode) sourceCourse.get();

    // Topics to be covered - the topis that should have been covered by the chain of courses.
    Set<String> topicsToBeCovered = getDependentTopicsForCourseFromTopicGraph(topicGraph, courseItem);

    INode currentNode = curriculumGraph.getNodeById(courseItem.getId());

    // recursively visit each node and gatehr the info.
    CourseCurriculumCheckerResult result = new CourseCurriculumCheckerResult(courseItem);
    bfs(currentNode, curriculumGraph, topicsToBeCovered, result);

    // add topic details
    topicsToBeCovered.forEach(topicToBeCovered -> {
      CourseCurriculumCheckerResult.TopicCoveredDetails topicCoveredDetails = new CourseCurriculumCheckerResult.TopicCoveredDetails();
      topicCoveredDetails.topic = topicToBeCovered;
      result.getCourseDetails().forEach(courseDetails -> {
        if(!topicCoveredDetails.covered){
          topicCoveredDetails.covered = courseDetails.coveredTopics.contains(topicToBeCovered);
        }
      });
      result.getTopicCoveredDetails().add(topicCoveredDetails);
    });

    return result;
  }

  private CoursePrerequisitesCheckerResult checkPrerequisitesProperlySetInternal(Graph<Relationship> topicGraph, Graph<Relationship> curriculumGraph, String courseTitle) throws Exception {
    CoursePrerequisitesCheckerResult result;

    // source course
    Optional<INode> sourceCourse = curriculumGraph.getNodes().stream().filter(a -> a.getId().equals(courseTitle)).findFirst();
    CourseItemNode courseItem = (CourseItemNode) sourceCourse.get();

    // Actual topics - the prescribed dependent topics based on the pre-exisitng dependency relationships.
    Set<String> actualTopics = getDepedentTopicsForCourseFromCourseDependencies(curriculumGraph, courseItem);

    // Expected topics - the topics that are supposed to be there for the course.
    Set<String> expectedTopics = getDependentTopicsForCourseFromTopicGraph(topicGraph, courseItem);

    // Compare actual topics and expected topics. If expected topics do not exist in actual topics, then they should be recommended.
    result = new CoursePrerequisitesCheckerResult(courseItem);
    expectedTopics.forEach(expectedTopic -> {
      CoursePrerequisitesCheckerResult.TopicDetails topicDetails = new CoursePrerequisitesCheckerResult.TopicDetails();
      topicDetails.topic = expectedTopic;
      topicDetails.exists = actualTopics.contains(expectedTopic);

      // If expected topic does not exist in actual topics
      // try to find if there are other course offered that match the expected topics.
      topicDetails.recommendedCourses = new ArrayList<>();
      if(!topicDetails.exists){
        curriculumGraph.getNodes().stream().filter(node -> !Objects.equals(node.getId(), courseItem.getId())).forEach(node -> {
          CourseItemNode courseItemNode = (CourseItemNode) node;
          if (courseItemNode.getImpliedTopics().contains(topicDetails.topic)) {
            topicDetails.recommendedCourses.add(courseItemNode);
          }
        });
      }

      result.getTopicDetails().add(topicDetails);
    });

    return result;
  }

  public AthabascaUniversityGraphBuilderResult buildKnowledgeGraphFromAthabascaCurriculum(String input, String discipline) throws Exception {
//    String input = "http://www.athabascau.ca/course/course-listings.php?/undergraduate/all/all";
    AthabascaUniversityGraphBuilder builder = new AthabascaUniversityGraphBuilder(this.config.getRepository());
    AthabascaUniversityGraphBuilderResult result = builder.createGraph(input, discipline);
    saveGraphAsJson(result.getCurriculumGraph(), this.config.getRepository().getDataFilePathString("curriculumGraph/AthabascaUniversity.json"));
    saveGraphAsJson(result.getKnowledgeGraph(), this.config.getRepository().getDataFilePathString("knowledgeGraph/AthabascaUniversity.json"));
    List<SimpleCourseItem> curriculum = result.getCourses();
    saveSimpleCourseItemsAsJson(curriculum, this.config.getRepository().getDataFilePathString("impliedTopics.simple/AthabascaUniversity.json"));
    return result;
  }

  private void saveSimpleCourseItemsAsJson(List<SimpleCourseItem> courses, final String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<SimpleCourseItem> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, SimpleCourseItem.class, null).saveAsJson(courses);
  }

  private void saveGraphAsJson(Graph<Relationship> graph, final String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<Graph<Relationship>> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, Graph.class, CourseItemNode[].class).saveAsJson(graph);
  }

  public CourseCurriculumCheckerResult checkPrerequisitesOfIndividualCoursePage(String topicModelJsonRaw, String courseTitleJsonRaw) throws Exception {
    Graph<Relationship> topicGraph = FileUtil.readGraphFromJsonString(topicModelJsonRaw, TopicNode[].class);
    Graph<Relationship> curriculumGraph = FileUtil.readGraphFromJson(this.config.getRepository().getDataFilePathString("curriculumGraph/AthabascaUniversity.json"), CourseItemNode[].class);
    return checkCurriculumProperlySetInternal(topicGraph, curriculumGraph, courseTitleJsonRaw);
  }

  public List<CoursePrerequisitesCheckerResult> checkAllCoursesInAcademicProgramPage(String topicModelJsonRaw) throws Exception {
    List<CoursePrerequisitesCheckerResult> results = new ArrayList<>();
    Graph<Relationship> topicGraph = FileUtil.readGraphFromJsonString(topicModelJsonRaw, TopicNode[].class);
    Graph<Relationship> curriculumGraph = FileUtil.readGraphFromJson(this.config.getRepository().getDataFilePathString("curriculumGraph/AthabascaUniversity.json"), CourseItemNode[].class);
    List<SimpleCourseItem> curriculum = FileUtil.readSimpleCourseItemsFromJson(this.config.getRepository().getDataFilePathString("impliedTopics.simple/AthabascaUniversity.json"));
    curriculum.forEach(course ->{
      try {
        results.add(checkPrerequisitesProperlySetInternal(topicGraph, curriculumGraph, course.title));
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
    return results;
  }
}
