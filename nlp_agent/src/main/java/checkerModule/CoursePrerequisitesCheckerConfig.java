package checkerModule;

import common.util.Repository;

public class CoursePrerequisitesCheckerConfig {
    private Repository repository;

    public CoursePrerequisitesCheckerConfig(Repository repository){

        this.repository = repository;
    }

    public Repository getRepository() {
        return repository;
    }
}
