/**
 * TopicIdentificationProcessor.java
 *
 * Author: Yu Sasaki
 *
 * This class provides a logic to identify topics from NLP and NER results, and produces impliedTopics property for each course.
 *
 * */
package topicIdentification;

import common.model.CourseItem;
import common.util.JsonResourceManager;
import common.util.StopwordsProvider;

import java.io.IOException;
import java.util.*;

public class TopicIdentificationProcessor {

  private TopicIdentificationProcessorConfig config;
  private StopwordsProvider stopwordsProvider;

  public TopicIdentificationProcessor(
      TopicIdentificationProcessorConfig config,
      StopwordsProvider stopwordsProvider) {
    this.config = config;
    this.stopwordsProvider = stopwordsProvider;
  }

  public void processTopicIdentification() throws Exception {
    List<CourseItem> courses = readCourseItemsFromJson(config.getInFileName());
    courses = processTopicIdentification(courses);
    saveCourseItemsAsJson(courses, config.getOutFileName());
  }

  public List<CourseItem> processTopicIdentification(List<CourseItem> courses) throws Exception {
    courses = implyTopicsFromModeledCourses(courses);
    return courses;
  }

  private List<CourseItem> readCourseItemsFromJson(String fileName) throws Exception {
    System.out.println("reading the courses from json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    return resourceManager.with(fileName, CourseItem[].class, null).readFromJson();
  }

  private void saveCourseItemsAsJson(List<CourseItem> courses, String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, CourseItem.class, null).saveAsJson(courses);
  }

  private List<CourseItem> implyTopicsFromModeledCourses(List<CourseItem> courses) throws IOException {
    System.out.println("performing topic identification...");

    List<String> stopwords = stopwordsProvider.getStopwords();

    // extract possible topics from 2-gram words using the
    courses.forEach(course -> {

      Set<String> topicCandidates = new HashSet<>();

      // Add NER if the intersection with the LDA matches.
      course.getTopicCategories().forEach(topicItems -> {
        topicItems.forEach(topicItem -> {
          course.getSearchedTexts()
              .stream()
              .filter(a -> !stopwords.contains(a))
              .filter(a -> a.contains(topicItem.getDescription()))
              .forEach(topicCandidates::add);
        });
      });

      // Add topics from LDA result if they are found in the course title.
      course.getTopicCategories().forEach(topicItems -> {
        topicItems.stream()
            .filter(topicItem -> !stopwords.contains(topicItem.getDescription()))
            .filter(topicItem -> course.getTitle().toLowerCase().contains(topicItem.getDescription()))
            .filter(topicItem -> !topicCandidates.stream().anyMatch(a -> a.contains(topicItem.getDescription())))
            .forEach(topicItem -> {
              topicCandidates.add(topicItem.getDescription());
            });
      });

      course.setImpliedTopics(Arrays.asList(topicCandidates.toArray(new String[]{})));
    });

    return courses;
  }
}
