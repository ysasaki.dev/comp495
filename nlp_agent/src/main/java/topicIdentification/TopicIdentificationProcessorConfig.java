/**
 * TopicIdentificationProcessorConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for TopicIdentificationProcessor.
 *
 * */
package topicIdentification;

public class TopicIdentificationProcessorConfig {

  private String inFileName;
  private String outFileName;

  public TopicIdentificationProcessorConfig(
      String inFileName,
      String outFileName
  ) {

    this.inFileName = inFileName;
    this.outFileName = outFileName;
  }

  public String getInFileName() {
    return inFileName;
  }

  public String getOutFileName() {
    return outFileName;
  }
}
