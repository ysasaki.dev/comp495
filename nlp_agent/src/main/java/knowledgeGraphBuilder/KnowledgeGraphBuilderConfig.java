/**
 * KnowledgeGraphBuilderConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for KnowledgeGraphBuilder.
 *
 * */
package knowledgeGraphBuilder;

public class KnowledgeGraphBuilderConfig {

  private String inFileName;
  private String outFileName;

  public KnowledgeGraphBuilderConfig(
      String inFileName,
      String outFileName
  ){

    this.inFileName = inFileName;
    this.outFileName = outFileName;
  }

  public String getInFileName() {
    return inFileName;
  }

  public String getOutFileName() {
    return outFileName;
  }
}
