/**
 * KnowledgeGraphBuilder.java
 *
 * Author: Yu Sasaki
 *
 * This class provides logic for building knowledge base (graph) given the information about coures with prerequisites information
 * Internall it calls KnowledgeBaseBuilder.
 *
 * */

package knowledgeGraphBuilder;

import common.graph.Graph;
import common.graph.Relationship;
import common.graph.impl.CourseItemNode;
import common.model.CourseItem;
import common.util.JsonResourceManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KnowledgeGraphBuilder {

  private KnowledgeGraphBuilderConfig config;
  private KnowledgeBaseBuilder knowledgeBaseBuilder;

  public KnowledgeGraphBuilder(
      KnowledgeGraphBuilderConfig config,
      KnowledgeBaseBuilder knowledgeBaseBuilder
  ) {
    this.config = config;
    this.knowledgeBaseBuilder = knowledgeBaseBuilder;
  }

  public void produceGraphFromExplicitPrerequisitesRelationship() throws Exception {
    List<CourseItem> courses = readCourseItemsFromJson(config.getInFileName());
    Graph<Relationship> graph = produceGraphFromExplicitPrerequisitesRelationship(courses);
    saveGraphAsJson(graph, config.getOutFileName());
  }

  public Graph<Relationship> produceGraphFromExplicitPrerequisitesRelationship(List<CourseItem> courses) throws Exception {
    Graph<Relationship> graph = buildGraphFromExplicitPrerequisitesRelationship(courses);
    return graph;
  }


  public void produceKnowledgeBase() throws Exception {
    String[] jsonFileNames = config.getInFileName().split(";");
    Map<String, List<CourseItem>> universityCourses = new HashMap<>();
    for (int i = 0; i < jsonFileNames.length; i++) {
      String jsonFileName = jsonFileNames[i];
      List<CourseItem> courses = readCourseItemsFromJson(jsonFileName);
      universityCourses.put(jsonFileName, courses);
    }
    Map<String, Graph<Relationship>> graphs = buildKnowledgeBase(universityCourses);
    graphs.forEach((universityName, graph) -> {
      saveGraphAsJson(graph, universityName);
    });
  }

  private List<CourseItem> readCourseItemsFromJson(String fileName) throws Exception {
    System.out.println("reading the courses from json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    return resourceManager.with(fileName, CourseItem[].class, null).readFromJson();
  }

  private void saveGraphAsJson(Graph<Relationship> graph, final String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<Graph<Relationship>> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, Graph.class, CourseItemNode[].class).saveAsJson(graph);
  }

  private Map<String, Graph<Relationship>> buildKnowledgeBase(Map<String, List<CourseItem>> universityCourses) {
    System.out.println("building knowledge base...");
    return knowledgeBaseBuilder.build(universityCourses);
  }

  private Graph<Relationship> buildGraphFromExplicitPrerequisitesRelationship(List<CourseItem> courses) {
    System.out.println("building a common.knowledgeGraphBuilder from explicit prerequisites relationships...");
    return knowledgeBaseBuilder.build(courses, Arrays.asList(Relationship.Explicit));
  }
}
