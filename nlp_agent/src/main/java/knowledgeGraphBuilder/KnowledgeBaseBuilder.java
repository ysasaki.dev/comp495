/**
 * KnowledgeBaseBuilder.java
 *
 * Author: Yu Sasaki
 *
 * This class provides logic for building knowledge base (graph) given the information about coures with prerequisites information
 *
 * */

package knowledgeGraphBuilder;

import common.graph.Edge;
import common.graph.Graph;
import common.graph.Relationship;
import common.graph.impl.CourseItemNode;
import common.model.CourseItem;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class KnowledgeBaseBuilder {

  public KnowledgeBaseBuilder() {
  }

  Graph<Relationship> build(List<CourseItem> courses, List<Relationship> useRelationships) {

    Graph<Relationship> graph = new Graph<>();

    // add nodes
    Set<CourseItemNode> nd = courses.stream()
        .map(CourseItemNode::new)
        .sorted(Comparator.comparing(CourseItemNode::getId))
        .collect(Collectors.toSet());

    Set<CourseItemNode> nodes = nd.stream().collect(Collectors.toCollection(() -> new TreeSet<>(new Comparator<CourseItemNode>() {
      @Override
      public int compare(CourseItemNode o1, CourseItemNode o2) {
        return o1.getId().compareTo(o2.getId());
      }
    })));

    nodes.forEach(graph::addNode);

    if (useRelationships.contains(Relationship.Explicit)) {

      nodes.forEach(fromCourse -> {

        nodes.stream().filter(n -> !n.equals(fromCourse)).forEach(toCourse -> {

          Edge<Relationship> edge = null;

          List<String> explicit = findFromPrerequisites(fromCourse, toCourse);
          if (explicit.size() > 0) {
            edge = new Edge<>(fromCourse, toCourse, Relationship.Explicit, explicit);
          }

          if (edge != null) {
            graph.addEdge(edge);
          }

        });

      });

    }

    if (useRelationships.contains(Relationship.Implicit)) {

      nodes.forEach(fromCourse -> {

        nodes.stream()
            .filter(n -> !n.equals(fromCourse))
            .forEach(toCourse -> {

              Edge<Relationship> edge = null;

              List<String> implicit = findFromTopics(fromCourse, toCourse);
              if (implicit.size() > 0) {
                edge = new Edge<>(fromCourse, toCourse, Relationship.Implicit, implicit);
              }

              if (edge != null && !graph.hasEdge(edge, Relationship.Explicit)) {
                graph.addEdge(edge);
              }

            });

      });

    }

    return graph;

  }

  private ArrayList<String> findFromPrerequisites(CourseItemNode fromCourse, CourseItemNode toCourse) {

    ArrayList<String> reasons = new ArrayList<>();

    // capture the prerequisite course numbers from prerequisites description
    List<String> extractedCourseNumbers = new ArrayList<>();
    Pattern p = Pattern.compile("(\\d{3,})");
    Matcher m = p.matcher(fromCourse.getItem().getPrerequisites());
    while (m.find()) {
      extractedCourseNumbers.add(m.group());
    }
    if (extractedCourseNumbers.stream().anyMatch(n -> toCourse.getId().contains(n))) {
      reasons.add("Because one of " + String.join(", ", extractedCourseNumbers) + " matches to the course title");
    }

    return reasons;
  }

  private ArrayList<String> findFromTopics(CourseItemNode fromCourse, CourseItemNode toCourse) {

    ArrayList<String> clues = new ArrayList<>();

    fromCourse.getItem().getImpliedTopics().forEach(fromCourseTopic -> {

      if (fromCourse.getId().compareTo(toCourse.getId()) > 0) {

        toCourse.getItem().getMatchedTexts().forEach(matchedText -> {
          if (Objects.equals(fromCourseTopic, matchedText)) {
            clues.add("Because the word '" + fromCourseTopic + "' is included in the matched texts");
          }
        });

      }

    });

    return clues;
  }

  Map<String, Graph<Relationship>> build(Map<String, List<CourseItem>> modelUniversityCourses) {

    // create a knowledgeGraphBuilder for each university based on the explicit prerequisite relationships
    Map<String, Graph<Relationship>> universityPrerequisitesGraph = new HashMap<>();
    modelUniversityCourses.forEach((universityName, courses) -> {
      Graph<Relationship> graph = build(courses, Arrays.asList(Relationship.Explicit));
      universityPrerequisitesGraph.put(universityName, graph);
    });
    return universityPrerequisitesGraph;
  }
}
