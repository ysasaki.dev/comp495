/**
 * Modeler.java
 *
 * Author: Yu Sasaki
 *
 * This class provides an abstarct class for modeling logic
 *
 * */

package naturalLanguageProcessing;

import common.model.TopicCategories;

public abstract class Modeler {
  public abstract void model() throws Exception;
  public abstract TopicCategories getTopics() throws Exception;
}
