/**
 * NLPProcessor.java
 *
 * Author: Yu Sasaki
 *
 * This class provides a NLP processing functionality.
 * It internally uses LDA modeler to perform modeling
 *
 * */
package naturalLanguageProcessing;

import common.model.CourseItem;
import common.model.TopicCategories;
import common.util.JsonResourceManager;
import common.util.StopwordsProvider;
import naturalLanguageProcessing.impl.LDAConfig;

import java.util.*;

public class NLPProcessor {

  private NLPProcessorConfig config;
  private ModelerFactory modelerFactory;
  private StopwordsProvider stopwordsProvider;

  public NLPProcessor(
      NLPProcessorConfig config,
      ModelerFactory modelerFactory,
      StopwordsProvider stopwordsProvider) {
    this.config = config;
    this.modelerFactory = modelerFactory;
    this.stopwordsProvider = stopwordsProvider;
  }

  public void processLDAModeling() throws Exception {
    List<CourseItem> courses = readCourseItemsFromJson(config.getInFileName());
    courses = processLDAModeling(courses);
    saveCourseItemsAsJson(courses, config.getOutFileName());
  }

  public List<CourseItem> processLDAModeling(List<CourseItem> courses) throws Exception {
    courses = modelCourses(courses);
    return courses;
  }

  private List<CourseItem> readCourseItemsFromJson(String fileName) throws Exception {
    System.out.println("reading the courses from json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    return resourceManager.with(fileName, CourseItem[].class, null).readFromJson();
  }

  private void saveCourseItemsAsJson(List<CourseItem> courses, String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, CourseItem.class, null).saveAsJson(courses);
  }

  private List<CourseItem> modelCourses(List<CourseItem> courses) throws Exception {
    System.out.println("performing a topic naturalLanguageProcessing...");

    for (CourseItem course : courses) {
      System.out.println("....processing " + course.getTitle());

      // create a LDA modeler
      List<String> inputs = Arrays.asList(course.getTitle(), course.getDescription());
      Integer numberOfTopics = 5;
      Modeler modeler = modelerFactory.create(new LDAConfig(inputs, numberOfTopics, config.getLdaFileName()), stopwordsProvider);

      // perform naturalLanguageProcessing
      modeler.model();

      // get topicCategories
      TopicCategories topicCategories = modeler.getTopics();

      // create a modeled course item result
      course.setTopicCategories(topicCategories);
    }

    return courses;
  }
}
