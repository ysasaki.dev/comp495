/**
 * ModelerFactory.java
 *
 * Author: Yu Sasaki
 *
 * This class provides a factory to instantiate a Modeler instance.
 *
 * */
package naturalLanguageProcessing;

import naturalLanguageProcessing.impl.LDA;
import naturalLanguageProcessing.impl.LDAConfig;
import common.util.StopwordsProvider;

public class ModelerFactory {

  public Modeler create(ModelerConfig modelerConfig, StopwordsProvider stopwordsProvider) throws Exception {

    switch (modelerConfig.getName()) {
      case "LDA": {
        return new LDA((LDAConfig) modelerConfig, stopwordsProvider);
      }
    }

    throw new Exception("No implementation for " + modelerConfig.getName());
  }
}
