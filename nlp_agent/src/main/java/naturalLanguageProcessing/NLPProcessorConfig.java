/**
 * NLPProcessorConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for NLPProcessor.
 *
 * */

package naturalLanguageProcessing;

public class NLPProcessorConfig {

  private String outFileName;
  private String inFileName;
  private String ldaFileName;

  public NLPProcessorConfig(
      String inFileName,
      String outFileName,
      String ldaFileName){

    this.inFileName = inFileName;
    this.outFileName = outFileName;
    this.ldaFileName = ldaFileName;
  }

  public String getOutFileName() {
    return outFileName;
  }

  public String getInFileName() {
    return inFileName;
  }

  public String getLdaFileName() {
    return ldaFileName;
  }

}
