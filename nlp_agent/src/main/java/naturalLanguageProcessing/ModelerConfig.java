/**
 * ModelerConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for Modeler.
 *
 * */

package naturalLanguageProcessing;

public class ModelerConfig {

  private String name;

  public ModelerConfig(String name){

    this.name = name;
  }

  public String getName() {
    return name;
  }
}
