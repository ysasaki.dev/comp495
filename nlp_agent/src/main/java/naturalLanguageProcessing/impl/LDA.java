/**
 * NLPProcessor.java
 *
 * Author: Yu Sasaki
 *
 * This class provides LDA implementation using MALLET library.
 *
 * References
 * - http://mallet.cs.umass.edu/topics-devel.php
 * - http://www.benmccann.com/latent-dirichlet-allocation-mallet/
 * - http://www.mimno.org/articles/phrases/
 * */

package naturalLanguageProcessing.impl;

import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.ArrayIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.*;
import naturalLanguageProcessing.Modeler;
import common.model.TopicCategories;
import common.model.TopicCategory;
import common.model.TopicItem;
import common.util.StopwordsProvider;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

public class LDA extends Modeler {

  private InstanceList instanceList;
  private LDAConfig config;
  private StopwordsProvider stopwordsProvider;

  public LDA(LDAConfig config, StopwordsProvider stopwordsProvider) {
    this.config = config;
    this.stopwordsProvider = stopwordsProvider;
  }

  private InstanceList createInstanceList(List<String> texts) throws IOException {
    ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

    pipeList.add(new CharSequenceLowercase());
    // \p{L} matches any letters from any language
    // removing words such as of, in, by, to, and numbers
    pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
    // remove stopwords
    TokenSequenceRemoveStopwords stopwords = new TokenSequenceRemoveStopwords();
    stopwords.addStopWords(stopwordsProvider.getStopwords().toArray(new String[]{}));
    pipeList.add(stopwords);
    // Convert token sequence into feature sequence
    pipeList.add(new TokenSequence2FeatureSequence());

    instanceList = new InstanceList(new SerialPipes(pipeList));

    instanceList.addThruPipe(new ArrayIterator(texts));
    return instanceList;
  }

  private ParallelTopicModel createNewModel() throws IOException {
    // read input file
    // List<String> texts = ResourceManager.readAllLines(input);
    InstanceList instances = createInstanceList(config.getInputs());

    // estimate the model
    ParallelTopicModel model = new ParallelTopicModel(config.getNumberOfTopics(), 1.0, 0.01);
    model.addInstances(instances);
    model.setNumThreads(1);
    model.setNumIterations(50);
    model.estimate();

    return model;
  }

  private ParallelTopicModel getOrCreateModel() throws Exception {
    ParallelTopicModel model = createNewModel();
    return model;
  }

  public void model() throws Exception {
    getOrCreateModel();
  }

  @Override
  public TopicCategories getTopics() throws Exception {
    TopicCategories categories = new TopicCategories();

    ParallelTopicModel model = getOrCreateModel();
    Alphabet dataAlphabet = instanceList.getDataAlphabet();

    ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();

    for (int topic = 0; topic < config.getNumberOfTopics(); topic++) {
      Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
      int rank = 0;
      TopicCategory topicCategory = new TopicCategory();
      while (iterator.hasNext() && rank < 5) {
        IDSorter idCountPair = iterator.next();
        String object = (String) dataAlphabet.lookupObject(idCountPair.getID());
        Double weight = idCountPair.getWeight();
        TopicItem topicItem = new TopicItem(object, rank, weight);
        topicCategory.add(topicItem);
        rank++;
      }
      categories.add(topicCategory);
    }

    return categories;
  }

}
