/**
 * LDAConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for LDA.
 *
 * */

package naturalLanguageProcessing.impl;

import naturalLanguageProcessing.ModelerConfig;

import java.util.List;

public class LDAConfig extends ModelerConfig {

  private List<String> inputs;
  private Integer numberOfTopics;
  private String outputFilePath;

  public LDAConfig(List<String> inputs, Integer numberOfTopics, String outputFilePath) {
    super("LDA");
    this.inputs = inputs;
    this.numberOfTopics = numberOfTopics;
    this.outputFilePath = outputFilePath;
  }

  public List<String> getInputs() {
    return inputs;
  }

  public Integer getNumberOfTopics() {
    return numberOfTopics;
  }

  public String getOutputFilePath() {
    return outputFilePath;
  }
}
