/**
 * Main.java
 *
 * Author: Yu Sasaki
 *
 * Provides REST endpoints so that web frontend can interact with the componetns.
 *
 * */
import checkerModule.CourseCurriculumCheckerResult;
import common.util.Repository;
import common.util.ResourceManager;
import namedEntityRecognition.wikiDb.WikiDbImporter;
import namedEntityRecognition.wikiDb.WikiDbImporterConfig;
import namedEntityRecognition.wikiDb.WikiDbRepositoryConfig;
import org.json.JSONObject;
import checkerModule.Checker;
import checkerModule.CoursePrerequisitesCheckerConfig;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPInputStream;

import com.google.gson.Gson;
import spark.ResponseTransformer;
import io.github.cdimascio.dotenv.Dotenv;

import static spark.Spark.*;

public class Main {

    static ConcurrentHashMap<Integer, String[]> queue = new ConcurrentHashMap<>();
    static boolean wikiImportingJobLock = false;

    public static class JsonTransformer implements ResponseTransformer {

        private Gson gson = new Gson();

        @Override
        public String render(Object model) {
            return gson.toJson(model);
        }

    }

    // Enables CORS on requests. This method is an initialization method and should be called once.
    private static void enableCORS() {

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Request-Method", "*");
            response.type("application/json");
        });
    }

    private static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }

    public static void main(String[] args) throws IOException {

        final Dotenv dotenv = Dotenv.load();
        final String root = dotenv.get("root");
        final String bin = root + dotenv.get("bin");
        final String datapath = root + dotenv.get("data");
        final String wikidumpurl = dotenv.get("wikidumpurl");

        final String leveldbpath = bin + "leveldb";
        final String wikiTitleFilePath = bin + "/enwiki-latest-all-titles-in-ns0";
        final String saveFileName = bin + Paths.get(wikidumpurl).getFileName();

        final Repository repo = new Repository(leveldbpath, datapath);

        System.out.println("bin: " + bin);

        port(getHerokuAssignedPort());

        enableCORS();

        get("/ping", (req, res) -> "Hello World");

        get("/topicModels", (req, res) -> {
            res.type("application/json");
            return String.join("", repo.readDataString("bayesGraph/AthabascaUniversity.json"));
        });

        get("/courses", (req, res) -> {
            res.type("application/json");
            return String.join("", repo.readDataString("impliedTopics.simple/AthabascaUniversity.json"));
        });

        get("/queue", (req, res) -> queue, new JsonTransformer());

        get("/downloadwikidb", (req, res) -> {
            System.out.println("Donloading wiki dump file...");

            try{
                if (Files.exists(Paths.get(saveFileName))) {
                    Files.delete(Paths.get(saveFileName));
                }

                URL website = new URL(wikidumpurl);
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                FileOutputStream fos = new FileOutputStream(saveFileName);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

            } catch (Exception ex){
                ex.printStackTrace();
            }

            return "Downloaded to: " + saveFileName;
        });

        get("/unzipwikidb", (req, res) -> {
            System.out.println("Unzipping wiki dump file...");

            try {

                if (Files.exists(Paths.get(wikiTitleFilePath))) {
                    Files.delete(Paths.get(wikiTitleFilePath));
                }

                byte[] buffer = new byte[1024];
                try (GZIPInputStream in = new GZIPInputStream(new FileInputStream(saveFileName))) {
                    File targetFile = new File(wikiTitleFilePath);
                    try (OutputStream outStream = new FileOutputStream(targetFile)) {
                        int len;
                        while ((len = in.read(buffer)) > 0) {
                            outStream.write(buffer, 0, len);
                        }
                        outStream.write(buffer);
                    }
                }


                return "Unzipped to: " + wikiTitleFilePath;

            }catch (Exception ex){

                return "Failed:" + ex.toString();

            }

        });

        get("/populatewikidb", (req, res) -> {
            if(wikiImportingJobLock){
                return "Cannot start a new importing job. Another job is running... please wait.";
            }
            String stopwordsListFilePath = ResourceManager.get("stoplists/en.txt").toString();

            if (Files.exists(Paths.get(leveldbpath))) {
                Files.delete(Paths.get(leveldbpath));
            }

            WikiDbRepositoryConfig wikiDbRepositoryConfig = new WikiDbRepositoryConfig(leveldbpath);
            WikiDbImporterConfig config = new WikiDbImporterConfig(stopwordsListFilePath, wikiTitleFilePath, wikiDbRepositoryConfig);
            WikiDbImporter wikiDbImporter = new WikiDbImporter(config);

            Thread thread = new Thread(){
                public void run() {
                    try {
                        wikiImportingJobLock = true;
                        System.out.println("importing...");
                        wikiDbImporter.populateDb();
                        wikiImportingJobLock = false;
                        System.out.println("Done.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();
            return "Importing job started";
        });

        post("/buildKnowledgeGraph", (req, res) -> {
            final String PENDING_STATUS = "Executing...";
            String body = req.body();
            final JSONObject obj = new JSONObject(body);
            final String input = obj.getString("input");
            final String discipline = obj.getString("discipline");

            Thread thread = new Thread(){
                public void run() {
                    Integer next = 0;
                    Optional<Integer> val = queue.keySet().stream().max(Comparator.comparing(Integer::valueOf));
                    if (val.isPresent()) {
                        next = val.get() + 1;
                    }
                    queue.put(next, new String[]{input, new Date().toString(), PENDING_STATUS, "", ""});
                    CoursePrerequisitesCheckerConfig config = new CoursePrerequisitesCheckerConfig(repo);
                    Checker checker = new Checker(config);
                    try {
                        checker.buildKnowledgeGraphFromAthabascaCurriculum(input, discipline);
                        queue.get(next)[2] = "Successful";
                        queue.get(next)[3] = new Date().toString();
                    } catch (Exception e) {
                        queue.get(next)[2] = "Failed";
                        queue.get(next)[3] = new Date().toString();
                        queue.get(next)[4] = e.toString();
                        e.printStackTrace();
                    }
                }
            };

            Optional<Integer> val = queue.keySet().stream().max(Comparator.comparing(Integer::valueOf));
            if (val.isPresent()) {
                if (queue.get(val.get())[2] == PENDING_STATUS) {
                    return false;
                }
            }

            thread.start();

            return true;
        }, new JsonTransformer());

        post("/checkAllCoursesInAcademicProgramPage", (req, res) -> {
            String body = req.body();

            final JSONObject obj = new JSONObject(body);
            final String topicModel = obj.getJSONObject("topicModel").toString();

            CoursePrerequisitesCheckerConfig config = new CoursePrerequisitesCheckerConfig(repo);
            Checker checker = new Checker(config);
            return checker.checkAllCoursesInAcademicProgramPage(topicModel);
        }, new JsonTransformer());

        post("/checkPrerequisitesOfIndividualCoursePage", (req, res) -> {
            String body = req.body();

            final JSONObject obj = new JSONObject(body);
            final String topicModel = obj.getJSONObject("topicModel").toString();
            final String input = obj.getString("input");

            CoursePrerequisitesCheckerConfig config = new CoursePrerequisitesCheckerConfig(repo);
            Checker checker = new Checker(config);
            CourseCurriculumCheckerResult result = checker.checkPrerequisitesOfIndividualCoursePage(topicModel, input);
            return result;
        }, new JsonTransformer());
    }
}