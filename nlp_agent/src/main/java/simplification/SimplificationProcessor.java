/**
 * SimplificationProcessor.java
 *
 * Author: Yu Sasaki
 *
 * This class provides a logic to simplify course json file by eliminating NLP and NER results,
 * and leaving only impliedTopics as a property for each course
 *
 * */
package simplification;

import common.model.CourseItem;
import common.model.SimpleCourseItem;
import common.util.JsonResourceManager;

import java.util.List;
import java.util.stream.Collectors;

public class SimplificationProcessor {

  private SimplificationProcessorConfig config;

  public SimplificationProcessor(
      SimplificationProcessorConfig config
  ) {
    this.config = config;
  }

  public void produceSimpleCourseModels() throws Exception {
    List<CourseItem> courses = readCourseItemsFromJson(config.getInFileName());
    List<SimpleCourseItem> simpleCourses = produceSimpleCourseModels(courses);
    saveSimpleCourseItemsAsJson(simpleCourses, config.getOutFileName());
  }

  public List<SimpleCourseItem> produceSimpleCourseModels(List<CourseItem> courses) throws Exception {
    List<SimpleCourseItem> simpleCourses = courses.stream().map(SimpleCourseItem::copyFromCourseItem).collect(Collectors.toList());
    return simpleCourses;
  }

  private List<CourseItem> readCourseItemsFromJson(String fileName) throws Exception {
    System.out.println("reading the courses from json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    return resourceManager.with(fileName, CourseItem[].class, null).readFromJson();
  }

  private void saveSimpleCourseItemsAsJson(List<SimpleCourseItem> courses, final String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<SimpleCourseItem> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, SimpleCourseItem.class, null).saveAsJson(courses);
  }
}
