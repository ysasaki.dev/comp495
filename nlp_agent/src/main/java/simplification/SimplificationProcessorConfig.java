/**
 * SimplificationProcessorConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for SimplificationProcessor.
 *
 * */

package simplification;

public class SimplificationProcessorConfig {

  private String inFileName;
  private String outFileName;

  public SimplificationProcessorConfig(
      String inFileName,
      String outFileName
  ){

    this.inFileName = inFileName;
    this.outFileName = outFileName;
  }

  public String getInFileName() {
    return inFileName;
  }

  public String getOutFileName() {
    return outFileName;
  }
}
