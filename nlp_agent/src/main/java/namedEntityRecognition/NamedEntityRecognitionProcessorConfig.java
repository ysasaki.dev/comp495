/**
 * NamedEntityRecognitionProcessor.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for NamedEntityRecognitionProcessor.
 *
 * */

package namedEntityRecognition;

public class NamedEntityRecognitionProcessorConfig {

  private String inFileName;
  private String outFileName;

  public NamedEntityRecognitionProcessorConfig(
      String inFileName,
      String outFileName){

    this.inFileName = inFileName;
    this.outFileName = outFileName;
  }

  public String getInFileName() {
    return inFileName;
  }

  public String getOutFileName() {
    return outFileName;
  }
}
