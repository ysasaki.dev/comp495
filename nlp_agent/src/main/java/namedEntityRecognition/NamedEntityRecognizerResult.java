/**
 * NamedEntityRecognizerResult.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a Result model for NamedEntityRecognizer.
 *
 * */

package namedEntityRecognition;

public class NamedEntityRecognizerResult {

  private String searchedText;
  private String matchText;
  private Boolean result;

  public void setFound(String searchedText, String matchText){
    this.searchedText = searchedText;
    this.matchText = matchText;
    this.result = true;
  }

  public boolean found(){
    return result;
  }

  public String getSearchedText(){
    return searchedText;
  }

  public String getMatchedText(){
    return matchText;
  }

  @Override
  public String toString() {
    return "'" + searchedText + "' => " + (result ? "found <= '" + matchText + "'" : "not found");
  }
}
