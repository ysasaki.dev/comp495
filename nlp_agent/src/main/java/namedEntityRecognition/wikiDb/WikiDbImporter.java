/**
 * WikiDbImporter.java
 *
 * Author: Yu Sasaki
 *
 * This class imports Wikipedia dump into level key-value database.
 *
 * */

package namedEntityRecognition.wikiDb;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class WikiDbImporter {

  private final WikiDbImporterConfig config;

  public WikiDbImporter(WikiDbImporterConfig config) throws IOException {
    this.config = config;
  }

  public void populateDb() throws IOException {

    final List<String> stopwords = Files.readAllLines(Paths.get(config.getStopwordsListFilePath()));
    String fileName = config.getWikiTitleFilePath();

    try (Stream<String> stream = Files.lines(Paths.get(fileName));
         WikiDbRepository repo = new WikiDbRepository(config.getWikiDbRepositoryConfig())) {

      stream.forEach((line) -> {
        if (line.isEmpty()) {
          return;
        }
        for (String stopword : stopwords) {
          if (line.startsWith(stopword + "_") || line.matches("^\\d+.*")) {
            return;
          }
        }
        repo.insert(line, "1");
      });

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
