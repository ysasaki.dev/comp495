/**
 * WikiDbRepository.java
 *
 * Author: Yu Sasaki
 *
 * This class provides access to Wikipedia-imported data in the LevelDB storage to the outside world
 *
 * */
package namedEntityRecognition.wikiDb;

import org.iq80.leveldb.DB;
import org.iq80.leveldb.DBIterator;
import org.iq80.leveldb.Options;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.iq80.leveldb.impl.Iq80DBFactory.asString;
import static org.iq80.leveldb.impl.Iq80DBFactory.bytes;
import static org.iq80.leveldb.impl.Iq80DBFactory.factory;

public class WikiDbRepository implements Closeable {

  private WikiDbRepositoryConfig config;

  private DB db;

  public WikiDbRepository(WikiDbRepositoryConfig config) throws IOException {

    this.config = config;
    openDb();
  }

  private void openDb() throws IOException {
    Options options = new Options();
    options.createIfMissing(true);
    db = factory.open(new File(config.getDbFilePath()), options);
  }

  public void insert(String key, String value){
    db.put(bytes(key), bytes(value));
  }

  public Map<String, Map<String, String>> treeSearch(Map<String, List<String>> titleWithSearchTexts) {

    Map<String, Map<String, String>> foundPatternsPerTitle = new HashMap<>();

    TreeMap<String, String> treeMap = new TreeMap<>();
    try (DBIterator iterator = db.iterator()) {
      for (iterator.seekToFirst(); iterator.hasNext(); iterator.next()) {
        String key = asString(iterator.peekNext().getKey());
        treeMap.put(key.toLowerCase(), "1");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    titleWithSearchTexts.forEach((title, searchTexts) -> {

      Map<String, String> foundPatternTable = new Hashtable<>();

      for (String searchText : searchTexts) {
        if (!foundPatternTable.containsKey(searchText)) {
          String value = treeMap.get(searchText.toLowerCase());
          if (value != null && !value.isEmpty()) {
            foundPatternTable.put(searchText, searchText);
          }
        }
      }

      if (foundPatternsPerTitle.containsKey(title)) {
        Map<String, String> exitingData = foundPatternsPerTitle.get(title);
        exitingData.putAll(foundPatternTable);
      } else {
        foundPatternsPerTitle.put(title, foundPatternTable);
      }
    });

    return foundPatternsPerTitle;
  }

  @Override
  public void close() throws IOException {
    db.close();
  }
}
