/**
 * WikiDbRepositoryConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for WikiDbRepository.
 *
 * */
package namedEntityRecognition.wikiDb;

public class WikiDbRepositoryConfig {

  private String dbFilePath;

  public WikiDbRepositoryConfig(String dbFilePath){

    this.dbFilePath = dbFilePath;
  }

  public String getDbFilePath() {
    return dbFilePath;
  }
}
