/**
 * WikiDbImporterConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for WikiDbImporter.
 *
 * */
package namedEntityRecognition.wikiDb;

public class WikiDbImporterConfig {
  private String stopwordsListFilePath;
  private String wikiTitleFilePath;
  private WikiDbRepositoryConfig wikiDbRepositoryConfig;

  public WikiDbImporterConfig(String stopwordsListFilePath, String wikiTitleFilePath, WikiDbRepositoryConfig wikiDbRepositoryConfig){

    this.stopwordsListFilePath = stopwordsListFilePath;
    this.wikiTitleFilePath = wikiTitleFilePath;
    this.wikiDbRepositoryConfig = wikiDbRepositoryConfig;
  }

  public String getStopwordsListFilePath() {
    return stopwordsListFilePath;
  }

  public String getWikiTitleFilePath() {
    return wikiTitleFilePath;
  }

  public WikiDbRepositoryConfig getWikiDbRepositoryConfig() {
    return wikiDbRepositoryConfig;
  }
}
