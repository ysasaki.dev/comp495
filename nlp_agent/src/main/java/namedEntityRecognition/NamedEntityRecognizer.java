/**
 * NamedEntityRecognizer.java
 *
 * Author: Yu Sasaki
 *
 * This class performs Named Entity Recognition using Wikipedia as a data source.
 *
 * */


package namedEntityRecognition;

import common.model.CourseItem;
import namedEntityRecognition.wikiDb.WikiDbRepository;

import java.io.IOException;
import java.util.*;

public class NamedEntityRecognizer {

  private NamedEntityRecognizerConfig config;

  public NamedEntityRecognizer(NamedEntityRecognizerConfig config) throws IOException {
    this.config = config;
  }

  public Map<CourseItem, List<NamedEntityRecognizerResult>> searchAllCourses(final Map<CourseItem, Set<String>> courseAndSearchPatternTokens) {

    final Map<String, List<String>> courseAndSearchPatternTokensMap = new HashMap<>();

    courseAndSearchPatternTokens.forEach((course, searchPatternTokens) -> {
      List<String> searchPatterns = new ArrayList<>();
      for (String searchPatternToken : searchPatternTokens) {
        searchPatterns.add(searchPatternToken);
      }
      courseAndSearchPatternTokensMap.put(course.getTitle(), searchPatterns);
    });

    Map<CourseItem, List<NamedEntityRecognizerResult>> resultMap = new HashMap<>();

    try (WikiDbRepository repo = new WikiDbRepository(config.getWikiDbRepositoryConfig())) {

      System.out.println("....searching");

      Map<String, Map<String, String>> titleWithFoundPatternTable = repo.treeSearch(courseAndSearchPatternTokensMap);

      titleWithFoundPatternTable.forEach((courseTitle, foundPatterntable) -> {
        System.out.println("...." + courseTitle);

        List<NamedEntityRecognizerResult> results = new ArrayList<>();

        foundPatterntable.forEach((searchedPattern, matchedPattern) -> {
          NamedEntityRecognizerResult result = new NamedEntityRecognizerResult();
          System.out.println("....found pattern: " + searchedPattern);
          result.setFound(searchedPattern, matchedPattern);
          results.add(result);
        });

        Optional<CourseItem> courseOptional = courseAndSearchPatternTokens.keySet().stream().filter(a -> Objects.equals(a.getTitle(), courseTitle)).findFirst();
        courseOptional.ifPresent(courseItem -> resultMap.put(courseItem, results));
      });

    } catch (Exception e) {
      e.printStackTrace();
    }

    return resultMap;
  }
}
