/**
 * NamedEntityRecognizerConfig.java
 *
 * Author: Yu Sasaki
 *
 * This class represents a config model for NamedEntityRecognizer.
 *
 * */

package namedEntityRecognition;

import namedEntityRecognition.wikiDb.WikiDbRepositoryConfig;

public class NamedEntityRecognizerConfig {

  private WikiDbRepositoryConfig wikiDbRepositoryConfig;

  public NamedEntityRecognizerConfig(WikiDbRepositoryConfig wikiDbRepositoryConfig){

    this.wikiDbRepositoryConfig = wikiDbRepositoryConfig;
  }

  public WikiDbRepositoryConfig getWikiDbRepositoryConfig() {
    return wikiDbRepositoryConfig;
  }
}
