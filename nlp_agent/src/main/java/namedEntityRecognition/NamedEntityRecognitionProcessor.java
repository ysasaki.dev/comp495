/**
 * NamedEntityRecognitionProcessor.java
 *
 * Author: Yu Sasaki
 *
 * This class performs Named Entity Recognition using Wikipedia as a data source.
 * It internally uses NamedEntityRecognizer class
 *
 * */

package namedEntityRecognition;

import common.model.CourseItem;
import common.util.JsonResourceManager;
import common.util.StopwordsProvider;
import common.util.Tokenizer;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class NamedEntityRecognitionProcessor {

  private NamedEntityRecognitionProcessorConfig config;
  private NamedEntityRecognizer namedEntityRecognizer;
  private Tokenizer tokenizer;
  private StopwordsProvider stopwordsProvider;

  public NamedEntityRecognitionProcessor(
      NamedEntityRecognitionProcessorConfig config,
      NamedEntityRecognizer namedEntityRecognizer,
      Tokenizer tokenizer,
      StopwordsProvider stopwordsProvider) {
    this.config = config;
    this.namedEntityRecognizer = namedEntityRecognizer;
    this.tokenizer = tokenizer;
    this.stopwordsProvider = stopwordsProvider;
  }

  public void processNERIdentification() throws Exception {
    List<CourseItem> courses = readCourseItemsFromJson(config.getInFileName());
    courses = processNERIdentification(courses);
    saveCourseItemsAsJson(courses, config.getOutFileName());
  }

  public List<CourseItem> processNERIdentification(List<CourseItem> courses) throws Exception {
    courses = identifyNerFromCourses(courses);
    return courses;
  }

  private List<CourseItem> readCourseItemsFromJson(String fileName) throws Exception {
    System.out.println("reading the courses from json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    return resourceManager.with(fileName, CourseItem[].class, null).readFromJson();
  }

  private void saveCourseItemsAsJson(List<CourseItem> courses, String fileName) {
    System.out.println("saving into json file: " + fileName + " ...");
    JsonResourceManager<CourseItem> resourceManager = new JsonResourceManager<>();
    resourceManager.with(fileName, CourseItem.class, null).saveAsJson(courses);
  }

  private List<CourseItem> identifyNerFromCourses(List<CourseItem> courses) throws IOException {
    System.out.println("performing NER identification...");

    // take into account the english stopwords.
    tokenizer.withStopwords(stopwordsProvider);

    Map<CourseItem, Set<String>> input = new HashMap<>();

    System.out.println("....creating input");

    for (CourseItem course : courses) {
      // get 2-gram tokens from the description
      String token = course.getTitle() + course.getDescription();
      // Set<String> tokens1gram = tokenizer.tokenize1gram(token);
      Set<String> tokens2gram = tokenizer.tokenize2gram(token);
      Set<String> tokens3gram = tokenizer.tokenize3gram(token);

      Set<String> tokens = new HashSet<>();
      // tokens.addAll(tokens1gram);
      tokens.addAll(tokens2gram);
      tokens.addAll(tokens3gram);

      input.put(course, tokens);
    }

    System.out.println("....searching");

    // search the tokens in wiki and get a result that contains recognized named entities
    Map<CourseItem, List<NamedEntityRecognizerResult>> output = namedEntityRecognizer.searchAllCourses(input);

    output.forEach((course, nerResults) -> {
      System.out.println("....constructing output for: " + course.getTitle());

      // get found NERs to course item
      List<String> searchedTexts = nerResults.stream().filter(NamedEntityRecognizerResult::found).map(NamedEntityRecognizerResult::getSearchedText).collect(Collectors.toList());
      List<String> matchedTexts = nerResults.stream().filter(NamedEntityRecognizerResult::found).map(NamedEntityRecognizerResult::getMatchedText).collect(Collectors.toList());
      System.out.println("...." + matchedTexts.size() + " matches found");

      course.setNer(searchedTexts, matchedTexts);
    });

    return courses;
  }
}
