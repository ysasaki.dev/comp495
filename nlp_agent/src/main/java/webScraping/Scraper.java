/**
 * Scraper.java
 *
 * Author: Yu Sasaki
 *
 * An abstract class that provides infrastructural functionality for scraping the pages.
 *
 * */
package webScraping;

import common.model.CourseItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.*;

public abstract class Scraper {

  public class CourseItemCallable implements Callable<CourseItem> {

    private String url;
    private PrintStream printStream;

    CourseItemCallable(String url, PrintStream printStream) {
      this.url = url;
      this.printStream = printStream;
    }

    public CourseItem call() throws Exception {
      Element detailDoc;
      try {
        printStream.println("[Details] Connecting to " + this.url + "...");
        detailDoc = Jsoup.connect(this.url).get();
        printStream.println("[Details] Extracting details ...");
        return extractDetail(this.url, detailDoc).get(0);
      } catch (IOException e) {
        printStream.println("failed to read " + this.url);
        e.printStackTrace(printStream);
      }
      return null;
    }
  }

  public class CourseItemComparator implements Comparator<CourseItem> {
    public int compare(CourseItem o1, CourseItem o2) {
      return o1.getTitle().compareTo(o2.getTitle());
    }
  }

  private List<CourseItem> courses = Collections.synchronizedList(new ArrayList<CourseItem>());
  private PrintStream printStream = System.out;

  public void scrape() {
    courses = Collections.synchronizedList(new ArrayList<CourseItem>());
    String url = getRule().getStartUrl();
    scrapeThePage(url);
  }

  private void scrapeThePage(String url) {
    Document doc = null;

    try {
      printStream.println("[Main] Connecting to " + url + "...");
      doc = Jsoup.connect(url).get();
      printStream.println("[Main] Extracting links ...");

      // extract detail page link href and submit to executor.
      String linkExtractorRegex = getRule().getLinkExtractorRegex();
      String courseElementSelector = getRule().getCourseElementSelector();

      if (linkExtractorRegex != null && !linkExtractorRegex.isEmpty()) {
        courses = extractDetailsFromLinks(doc, linkExtractorRegex);
      } else if (courseElementSelector != null && !courseElementSelector.isEmpty()) {
        courses = extractDetailsFromCourseElements(url, doc, courseElementSelector);
      }

      String paginationSelector = getRule().getPaginationElementSelector();
      if (paginationSelector != null && !paginationSelector.isEmpty()) {
        Element nextPageLink = doc.selectFirst(paginationSelector);
        if(nextPageLink != null) {
          final String nextPageUrl = nextPageLink.attr("abs:href");
          scrapeThePage(nextPageUrl);
        }
      }

      // sort the objects
      Collections.sort(courses, new CourseItemComparator());

    } catch (IOException e) {
      e.printStackTrace(printStream);
    }
  }

  private List<CourseItem> extractDetailsFromCourseElements(String url, Document doc, String courseElementSelector){
    for(Element element : doc.select(courseElementSelector)) {
      if(!element.text().isEmpty()){
        courses.addAll(extractDetail(url, element));
      }
    }
    return courses;
  }

  private List<CourseItem> extractDetailsFromLinks(Document doc, String linkExtractorRegex){
    ExecutorService executorService = Executors.newFixedThreadPool(8);
    List<Future<CourseItem>> handles = new ArrayList<>();
    Future<CourseItem> handle;
    Set<String> detailUrls = new HashSet<>();

    Elements links = doc.select("a[href~=" + linkExtractorRegex + "]");
    for (Element link : links) {
      final String detailUrl = link.attr("abs:href");
      detailUrls.add(detailUrl);
    }
    for (String detailUrl : detailUrls) {
      handle = executorService.submit(new CourseItemCallable(detailUrl, printStream));
      handles.add(handle);
    }
    executorService.shutdown();

    // get the done objects.
    for (Future<CourseItem> h : handles) {
      try {
        courses.add(h.get());
      } catch (Exception e) {
        e.printStackTrace(printStream);
      }
    }
    return courses;
  }

  public void setPrintStream(PrintStream printStream) {
    this.printStream = printStream;
  }

  public List<CourseItem> getCourses() {
    return courses;
  }

  protected abstract List<CourseItem> extractDetail(String url, Element doc);

  public abstract Rule getRule();

  protected String[] splitPrerequisiteFromContent(String content){
    if (content.contains("Prerequisites")) {
      return content.split("Prerequisites:");
    } else if (content.contains("Prerequisite")) {
      return content.split("Prerequisite:");
    }
    return new String[]{content, ""};
  }

}
