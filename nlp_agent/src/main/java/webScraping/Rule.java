/**
 * Rule.java
 *
 * Author: Yu Sasaki
 *
 * This class represents extraction rule for scraper.
 *
 * */
package webScraping;

public class Rule {

  private String startUrl;

  private String linkExtractorRegex;

  private String courseElementSelector;

  private String paginationElementSelector;

  public String getCourseElementSelector() {
    return courseElementSelector;
  }

  public void setCourseElementSelector(String courseElementSelector) {
    this.courseElementSelector = courseElementSelector;
  }

  public String getLinkExtractorRegex() {
    return linkExtractorRegex;
  }

  public void setLinkExtractorRegex(String linkExtractorRegex) {
    this.linkExtractorRegex = linkExtractorRegex;
  }

  public String getStartUrl() {
    return startUrl;
  }

  public void setStartUrl(String startUrl) {
    this.startUrl = startUrl;
  }

  public String getPaginationElementSelector() {
    return paginationElementSelector;
  }

  public void setPaginationElementSelector(String paginationElementSelector) {
    this.paginationElementSelector = paginationElementSelector;
  }
}
