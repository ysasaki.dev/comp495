/**
 * PrincetonUniversityScraper.java
 *
 * Author: Yu Sasaki
 *
 * A concrete implementation of Scraper for University Of Texas At Austine curriculum page
 *
 * */
package webScraping.impl;

import common.model.CourseItem;
import org.jsoup.nodes.Element;
import webScraping.Rule;
import webScraping.Scraper;

import java.util.Collections;
import java.util.List;

public class UniversityOfTexasAtAustineScraper extends Scraper {

  public Rule getRule(){
    Rule rule = new Rule();
    rule.setStartUrl("https://www.cs.utexas.edu/undergraduate-program/academics/curriculum/courses");
    rule.setLinkExtractorRegex(".*/courses/\\d\\d\\d\\w*-\\w+");
    return rule;
  }

  protected List<CourseItem> extractDetail(String url, Element doc) {
    String title = doc.selectFirst("h1[id='page-title']").text();
    String content = doc.selectFirst("div[id='content']").text();
    String[] split = splitPrerequisiteFromContent(content);
    String description = split[0].trim();
    String prerequisites = split[1].trim();

    return Collections.singletonList(new CourseItem(url, title, prerequisites, description,null, null, null, null));
  }
}
