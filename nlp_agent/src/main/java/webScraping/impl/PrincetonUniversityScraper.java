/**
 * PrincetonUniversityScraper.java
 *
 * Author: Yu Sasaki
 *
 * A concrete implementation of Scraper for Princeton University curriculum page
 *
 * */
package webScraping.impl;

import common.model.CourseItem;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import webScraping.Rule;
import webScraping.Scraper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PrincetonUniversityScraper extends Scraper {

  public Rule getRule(){
    Rule rule = new Rule();
    rule.setStartUrl("https://www.cs.princeton.edu/courses/catalog");
    rule.setCourseElementSelector("div[id=\"course-catalog\"]");
    rule.setPaginationElementSelector("ul[class=\"pagination\"] > li[class=\"next\"] > a");
    return rule;
  }

  protected List<CourseItem> extractDetail(String url, Element doc) {
    List<CourseItem> courses = new ArrayList<>();

    Elements titleElements = doc.select("h2[class=\"course-name\"]");
    Elements contentElements = doc.select("div[class=\"course-body\"]");
    Iterator<Element> titleIterator = titleElements.iterator();
    Iterator<Element> contentIterator = contentElements.iterator();

    while(titleIterator.hasNext() && contentIterator.hasNext()) {
      String title = titleIterator.next().selectFirst("h2[class=\"course-name\"]").text();
      Element contentSelector = contentIterator.next().selectFirst("div[class=\"course-body\"]");
      String content = contentSelector.text();
      String prerequisites = "";
      Element prereqSelector = contentSelector.selectFirst("div[class=\"course-prereq\"]");
      if(prereqSelector != null){
        prerequisites = prereqSelector.text();
      }
      String description = content.replace(prerequisites, "").trim();
      courses.add(new CourseItem(url, title, prerequisites, description, null, null, null, null));
    }

    return courses;
  }
}
