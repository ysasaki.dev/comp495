/**
 * UniversityOfWashingtonScraper.java
 *
 * Author: Yu Sasaki
 *
 * A concrete implementation of Scraper for University Of Washington curriculum page
 *
 * */
package webScraping.impl;

import common.model.CourseItem;
import org.jsoup.nodes.Element;
import webScraping.Rule;
import webScraping.Scraper;

import java.util.Collections;
import java.util.List;

public class UniversityOfWashingtonScraper extends Scraper {

  public Rule getRule(){
    Rule rule = new Rule();
    rule.setStartUrl("https://www.washington.edu/students/crscat/cse.html");
    rule.setCourseElementSelector("a[name~=^cse\\d{1,}$]");
    return rule;
  }

  protected List<CourseItem> extractDetail(String url, Element doc) {
    String title = doc.selectFirst("b").text();
    String content = doc.text().replace(title, "");
    String[] split = splitPrerequisiteFromContent(content);
    String description = split[0].trim();
    String prerequisites = split[1].trim();
    return Collections.singletonList(new CourseItem(url, title, prerequisites, description, null, null, null, null));
  }
}
