/**
 * AthabascaUniversityScraper.java
 *
 * Author: Yu Sasaki
 *
 * A concrete implementation of Scraper for Athabasca University curriculum page
 *
 * */
package webScraping.impl;

import common.model.CourseItem;
import org.jsoup.nodes.Element;
import webScraping.Rule;
import webScraping.Scraper;

import java.util.Collections;
import java.util.List;

public class AthabascaUniversityScraper extends Scraper {

  private final Rule rule;

  public AthabascaUniversityScraper(String startUrl, String targetDiscipline) {
    this.rule = new Rule();
    rule.setStartUrl(startUrl); // "http://www.athabascau.ca/course/course-listings.php?/undergraduate/all/all"
    rule.setLinkExtractorRegex(".*/" + targetDiscipline + "/" + targetDiscipline + "[1234]\\d\\d.*");
  }

  public Rule getRule(){
    return this.rule;
  }

  protected List<CourseItem> extractDetail(String url, Element doc) {
    String title = doc.selectFirst("h1[id='content-title']").text();
    String description = doc.selectFirst("div[id='course-overview']").text();

    String [] prerequisiteSelectors = new String[]{
        "p[id='course-prerequisites']",
        "p[id='course-additional-header']"
    };

    String prerequisites = "";
    for(String prerequisiteSelector: prerequisiteSelectors) {
      Element prerequisite = doc.selectFirst(prerequisiteSelector);
      if(prerequisite != null){
        prerequisites = prerequisite.text();
        break;
      }
    }
    return Collections.singletonList(new CourseItem(url, title, prerequisites, description, null, null, null, null));
  }
}
