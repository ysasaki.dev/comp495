package common.model;

import java.util.List;

public class CourseItem {

  public String url;
  public String title;
  public String prerequisites;
  public String description;

  public List<String> searchedTexts;
  public List<String> matchedTexts;
  public List<TopicCategory> topicCategories;

  public List<String> impliedTopics;

  public CourseItem(String url, String title, String prerequisites, String description, List<TopicCategory> topicCategories, List<String> searchedTexts, List<String> matchedTexts, List<String> impliedTopics){

    this.url = url;
    this.title = title;
    this.prerequisites = prerequisites;
    this.description = description;
    this.topicCategories = topicCategories;
    this.searchedTexts = searchedTexts;
    this.matchedTexts = matchedTexts;
    this.impliedTopics = impliedTopics;
  }

  public static CourseItem copy(CourseItem old){
    return new CourseItem(old.getUrl(), old.getTitle(), old.getPrerequisites(), old.getDescription(), old.getTopicCategories(), old.getSearchedTexts(), old.getMatchedTexts(), old.getImpliedTopics());
  }

  public String getUrl() {
    return url;
  }

  public String getTitle() {
    return title;
  }

  public String getPrerequisites() {
    return prerequisites;
  }

  public String getDescription() {
    return description;
  }

  public List<String> getSearchedTexts() {
    return searchedTexts;
  }

  public List<String> getMatchedTexts() {
    return matchedTexts;
  }

  public List<TopicCategory> getTopicCategories() {
    return topicCategories;
  }

  public List<String> getImpliedTopics() {
    return impliedTopics;
  }

  public void setTopicCategories(TopicCategories topicCategories) {
    this.topicCategories = topicCategories;
  }

  public void setNer(List<String> searchedTexts, List<String> matchedTexts) {
    this.searchedTexts = searchedTexts;
    this.matchedTexts = matchedTexts;
  }

  public void setImpliedTopics(List<String> impliedTopics){
    this.impliedTopics = impliedTopics;
  }


}
