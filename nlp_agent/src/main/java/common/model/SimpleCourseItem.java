package common.model;

import java.util.List;

public class SimpleCourseItem {

  public String url;
  public String title;
  public String prerequisites;
  public String description;

  public List<String> impliedTopics;

  public SimpleCourseItem(String url, String title, String prerequisites, String description, List<String> impliedTopics){

    this.url = url;
    this.title = title;
    this.prerequisites = prerequisites;
    this.description = description;
    this.impliedTopics = impliedTopics;
  }

  public static SimpleCourseItem copyFromCourseItem(CourseItem old){
    return new SimpleCourseItem(old.getUrl(), old.getTitle(), old.getPrerequisites(), old.getDescription(), old.getImpliedTopics());
  }

  public String getUrl() {
    return url;
  }

  public String getTitle() {
    return title;
  }

  public String getPrerequisites() {
    return prerequisites;
  }

  public String getDescription() {
    return description;
  }

  public List<String> getImpliedTopics() {
    return impliedTopics;
  }

  public void setImpliedTopics(List<String> impliedTopics){
    this.impliedTopics = impliedTopics;
  }


}
