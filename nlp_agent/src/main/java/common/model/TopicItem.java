package common.model;

public class TopicItem {

  private String description;
  private Integer ranking;
  private Double weight;

  public TopicItem(String description, Integer ranking, Double weight) {

    this.description = description;
    this.ranking = ranking;
    this.weight = weight;
  }

  public String getDescription() {
    return description;
  }

  public Integer getRanking() {
    return ranking;
  }

  public Double getWeight() {
    return weight;
  }
}
