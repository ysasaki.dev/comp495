package common.graph.impl;

import common.graph.INode;

public class TopicNode implements INode<String> {

  private String id;

  private String label;

  public TopicNode() {
  }

  public TopicNode(String topic) {
    this.id = topic;
    this.label = topic;
  }

  @Override
  public String getId() {
    return this.id;
  }

  @Override
  public String getLabel() {
    return this.label;
  }

  @Override
  public String getItem() {
    return this.label;
  }
}
