package common.graph.impl;

import common.model.CourseItem;
import common.graph.INode;

import java.util.List;

public class CourseItemNode implements INode<CourseItem> {

  private transient CourseItem courseItem;

  private String id;

  private String label;

  private List<String> impliedTopics;

  public CourseItemNode() {
  }

  public CourseItemNode(CourseItem courseItem) {

    this.courseItem = courseItem;
    this.id = courseItem.getTitle();
    this.label = courseItem.getTitle();
    this.impliedTopics = courseItem.getImpliedTopics();
  }

  @Override
  public String getId() {
    return this.id;
  }

  @Override
  public String getLabel() {
    return this.label;
  }

  public List<String> getImpliedTopics() {
    return this.impliedTopics;
  }

  @Override
  public CourseItem getItem() {
    return courseItem;
  }
}
