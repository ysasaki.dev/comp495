package common.graph;

public interface INode<T> {

  T getItem();

  String getId();

  String getLabel();
}

