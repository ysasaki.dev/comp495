package common.graph;

import java.util.List;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class Graph<KRelationship> {

  private List<INode> nodes = new ArrayList<>();

  private List<Edge<KRelationship>> edges = new ArrayList<>();

  public Graph() {
  }

  public Graph(List<INode> nodes, List<Edge<KRelationship>> edges) {
    this.nodes = nodes;
    this.edges = edges;
  }

  public void addNode(INode node) {
    if (!nodes.contains(node)) {
      this.nodes.add(node);
    }
  }

  public void addEdge(Edge<KRelationship> edge) {
    this.edges.add(edge);
  }

  public void addEdge(Edge<KRelationship> edge, KRelationship rel) {
    if (!hasEdge(edge, rel) && !isAcyclic(edge.getSource(), edge.getTarget())) {
      this.edges.add(edge);
    }
  }

  private boolean isAcyclic(INode source, INode target) {
    if (Objects.equals(target.getId(), source.getId())) {
      return true;
    }
    // check its ancestors
    List<INode> ancestors = getAllTargetNodes(target);
    for (int i = 0; i < ancestors.size(); i++) {
      if (isAcyclic(source, ancestors.get(i))) {
        return true;
      }
    }
    return false;
  }

  public List<INode> getNodes() {
    return this.nodes;
  }

  public List<Edge<KRelationship>> getEdges() {
    return this.edges;
  }

  public boolean hasEdge(Edge<KRelationship> edge, KRelationship relationship) {
    return this.edges.stream()
        .anyMatch(e ->
            e.getSource().getId().equals(edge.getSource().getId()) &&
                e.getTarget().getId().equals(edge.getTarget().getId()) &&
                e.getRelationship().equals(relationship)
        );
  }

  public List<INode> getAllTargetNodes(INode s) {
    return getEdges().stream()
        .filter(e -> Objects.equals(e.getSource().getId(), s.getId()))
        .map(Edge::getTarget)
        .collect(Collectors.toList());
  }

  public List<INode> getAllSourceNodes(INode s) {
    return getEdges().stream()
        .filter(e -> Objects.equals(e.getTarget().getId(), s.getId()))
        .map(Edge::getSource)
        .collect(Collectors.toList());
  }

  public List<INode> getAllAdjacentNodes(INode s) {
    return getEdges().stream()
        .filter(e -> Objects.equals(e.getTarget().getId(), s.getId()))
        .map(Edge::getSource)
        .collect(Collectors.toList());
  }

  public INode getNodeById(String id) {
    Optional<INode> optional = this.getNodes().stream().filter(a -> Objects.equals(a.getId(), id)).findFirst();
    return optional.orElse(null);
  }

  public void removeNode(INode n) {
    nodes.remove(n);
  }
}
