package common.graph;

import java.util.List;

public class Edge<KRelationship> {

  private String id;
  private INode source;
  private INode target;
  private KRelationship relationship;
  private List<String> reasons;
  public Edge() {
  }

  public Edge(INode source, INode target, KRelationship relationship, List<String> reasons) {

    this.source = source;
    this.target = target;
    this.relationship = relationship;
    this.reasons = reasons;
    String rel = relationship.toString();
    this.id = source.getId() + "->" + target.getId() + " (" + rel + ")";
  }

  public String getId() {
    return id;
  }

  public INode getSource() {
    return source;
  }

  public KRelationship getRelationship() {
    return relationship;
  }

  public INode getTarget() {
    return target;
  }

  public List<String> getReasons() {
    return reasons;
  }
}
