package common.graph;

public enum Relationship {
  Implicit, Explicit
}
