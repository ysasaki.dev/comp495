package common.util;

import java.io.IOException;
import java.util.*;

public class Tokenizer {

  private StopwordsProvider stopwordsProvider;

  public Tokenizer withStopwords(StopwordsProvider stopwordsProvider) {
    this.stopwordsProvider = stopwordsProvider;
    return this;
  }

  private List<String> withoutStopWords(String sentence) throws IOException {
//    final List<String> stopwords = stopwordsProvider.getStopwords();
//    List<String> tokens = new ArrayList<>();
//    for (String token : sentence.split(" ")) {
//      if(!stopwords.contains(token.toLowerCase())){
//        tokens.add(token);
//      }
//    }
    List<String> tokens = Arrays.asList(sentence.split(" "));
    return tokens;
  }

  public String onlyWord(String word){
    String word1 = word.toLowerCase().replaceAll("[^\\w]", "").replaceAll("\\d", "");
    return word1;
  }

  public Set<String> tokenize1gram(String sentence) throws IOException {
    List<String> tokens = withoutStopWords(sentence);
    Set<String> searchPatterns = new HashSet<>();
    for (int i = 0; i < tokens.size(); i++) {
      String token1 = onlyWord(tokens.get(i));
      if (!token1.isEmpty()) {
        String searchPattern = token1;
        searchPatterns.add(searchPattern);
      }
    }
    return searchPatterns;
  }

  public Set<String> tokenize2gram(String sentence) throws IOException {
    List<String> tokens = withoutStopWords(sentence);
    Set<String> searchPatterns = new HashSet<>();
    for (int i = 1; i < tokens.size(); i++) {
      String token1 = onlyWord(tokens.get(i - 1));
      String token2 = onlyWord(tokens.get(i));
      if (!token1.isEmpty() && !token2.isEmpty()
          && token1.length() > 2 && token2.length() > 2) {
        String searchPattern = token1 + "_" + token2;
        searchPatterns.add(searchPattern);
      }
    }
    return searchPatterns;
  }

  public Set<String> tokenize3gram(String sentence) throws IOException {
    List<String> tokens = withoutStopWords(sentence);
    Set<String> searchPatterns = new HashSet<>();
    for (int i = 2; i < tokens.size(); i++) {
      String token1 = onlyWord(tokens.get(i - 2));
      String token2 = onlyWord(tokens.get(i - 1));
      String token3 = onlyWord(tokens.get(i));
      if (!token1.isEmpty() && !token2.isEmpty() && !token3.isEmpty()
          && token1.length() > 2 && token2.length() > 2 && token3.length() > 2) {
        String searchPattern = token1 + "_" + token2 + "_" + token3;
        searchPatterns.add(searchPattern);
      }
    }
    return searchPatterns;
  }

}
