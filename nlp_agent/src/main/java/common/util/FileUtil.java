package common.util;

import common.model.SimpleCourseItem;
import common.graph.Graph;
import common.graph.Relationship;

import java.lang.reflect.Type;
import java.util.List;

public class FileUtil {

  public static Graph<Relationship> readGraphFromJson(String fileName, Type nodeType) throws Exception {
//    if(!fileName.contains("knowledgeGraphBuilder")){
//      fileName = fileName.replace(".json", ".knowledgeGraphBuilder.json");
//    }
    System.out.println("reading the courses knowledgeGraphBuilder from json file: " + fileName + " ...");
    JsonResourceManager<Graph<Relationship>> resourceManager = new JsonResourceManager<>();
    return resourceManager.with(fileName, Graph.class, nodeType).readGraphFromJson();
  }

  public static Graph<Relationship> readGraphFromJsonString(String jsonString, Type nodeType) throws Exception {
    JsonResourceManager<Graph<Relationship>> resourceManager = new JsonResourceManager<>();
    return resourceManager.withJsonString(jsonString, Graph.class, nodeType).readGraphFromJsonString();
  }

  public static List<SimpleCourseItem> readSimpleCourseItemsFromJson(String fileName) throws Exception {
    System.out.println("reading the courses from json file: " + fileName + " ...");
    JsonResourceManager<SimpleCourseItem> resourceManager = new JsonResourceManager<>();
    return resourceManager.with(fileName, SimpleCourseItem[].class, null).readFromJson();
  }
}
