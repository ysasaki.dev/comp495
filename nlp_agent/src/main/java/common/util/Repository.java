package common.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Repository {

    private String leveldbPath;
    private String dataPath;

    public Repository(String leveldbPath, String dataPath){
        this.leveldbPath = leveldbPath;
        this.dataPath = dataPath;
    }

    public List<String> readDataString(String s) throws IOException {
        return Files.readAllLines(Paths.get(this.dataPath + s));
    }

    public String getDataFilePathString(String s) {
        return this.dataPath + s;
    }

    public String getLevelDbPathString() {
        return leveldbPath;
    }
}
