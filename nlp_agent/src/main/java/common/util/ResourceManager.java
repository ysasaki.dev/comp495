package common.util;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ResourceManager {

  public static Path get(String fileName) {
    ResourceManager main = new ResourceManager();
    ClassLoader classLoader = main.getClass().getClassLoader();
    return Paths.get( classLoader.getResource(fileName).getFile());
  }
}
