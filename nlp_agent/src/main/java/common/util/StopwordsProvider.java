package common.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class StopwordsProvider {

  private String[] stopwordFiles;

  public StopwordsProvider(String[] stopwordFiles) {

    this.stopwordFiles = stopwordFiles;
  }

  public List<String> getStopwords() throws IOException {
    Set<String> stopwords = new HashSet<>();

    for (String stopwordFile : stopwordFiles) {
      final List<String> words = Files.readAllLines(Paths.get(stopwordFile));
      stopwords.addAll(words);
    }

    List<String> result = new ArrayList<>(stopwords);

    return result;
  }
}
