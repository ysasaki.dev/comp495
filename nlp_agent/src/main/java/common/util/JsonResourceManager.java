package common.util;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import common.graph.Edge;
import common.graph.Graph;
import common.graph.INode;
import common.graph.Relationship;

import java.io.*;
import java.util.*;

import java.lang.reflect.Type;

public class JsonResourceManager<T> {

  public class JsonResourceManagerAction {
    private Type type;
    private Type nodeType;
    private String jsonFilePath;
    private String jsonString;

    JsonResourceManagerAction(Type type, String jsonFilePath, String jsonString){

      this.type = type;
      this.jsonFilePath = jsonFilePath;
      this.jsonString = jsonString;
    }

    JsonResourceManagerAction(Type type, Type nodeType, String jsonFilePath, String jsonString){

      this.type = type;
      this.nodeType = nodeType;
      this.jsonFilePath = jsonFilePath;
      this.jsonString = jsonString;
    }

    public void saveAsJson(final T item) {
      (new File(jsonFilePath).getParentFile()).mkdirs();
      try (Writer writer = new FileWriter(jsonFilePath)) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        gson.toJson(item, writer);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    public void saveAsJson(final List<T> items) {
      (new File(jsonFilePath).getParentFile()).mkdirs();
      try (Writer writer = new FileWriter(jsonFilePath)) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        gson.toJson(items, writer);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    public java.util.List<T> readFromJson() throws Exception {
      try (FileReader reader = new FileReader(jsonFilePath)) {
        return Arrays.asList(new Gson().fromJson(reader, type));
      } catch (IOException e) {
        e.printStackTrace();
      }
      throw new Exception();
    }

    private T readGraphFromJsonInternal(Reader reader){
      GsonBuilder gsonBuilder = new GsonBuilder();
      Type type = new TypeToken<Graph>() {
      }.getType();

      gsonBuilder.registerTypeAdapter(type, new JsonDeserializer<Graph>() {
        @Override
        public Graph deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
          JsonObject jsonObject = json.getAsJsonObject();
          JsonArray jsonNodes = (jsonObject).get("nodes").getAsJsonArray();

          final List<INode> courseItems = Arrays.asList(new Gson().fromJson(jsonNodes, nodeType));

          GsonBuilder edgeGsonBuilder = new GsonBuilder();
          Type edgeListType = new TypeToken<List<Edge<Relationship>>>(){}.getType();
          edgeGsonBuilder.registerTypeAdapter(edgeListType, new JsonDeserializer<List<Edge<Relationship>>>() {
            @Override
            public List<Edge<Relationship>> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
              List<Edge<Relationship>> edges = new ArrayList<>();
              JsonArray edgeArray = json.getAsJsonArray();
              for(int i =0;i<edgeArray.size();i++) {
                Edge<Relationship> edge = new Edge<>();
                JsonObject edgeObject = edgeArray.get(i).getAsJsonObject();
                String id = edgeObject.get("id").getAsString();
                String sourceId = edgeObject.get("source").getAsJsonObject().get("id").getAsString();
                String targetId = edgeObject.get("target").getAsJsonObject().get("id").getAsString();
                // String relationship = edgeObject.get("relationship").getAsString();
                Optional<INode> sourceNode = courseItems.stream().filter(c -> Objects.equals(c.getId(), sourceId)).findFirst();
                Optional<INode> targetNode = courseItems.stream().filter(c -> Objects.equals(c.getId(), targetId)).findFirst();
                if(sourceNode.isPresent() && targetNode.isPresent()){
                  edge = new Edge<>(sourceNode.get(), targetNode.get(), Relationship.Explicit, null);
                  edges.add(edge);
                } else {
                  System.out.println("Couldn't deserialize -> " + edgeObject.toString());
                }
              }
              return edges;
            }
          });
          JsonArray jsonEdges = (jsonObject).get("edges").getAsJsonArray();
          List<Edge<Relationship>> edgeItems = edgeGsonBuilder.create().fromJson(jsonEdges, edgeListType);
          return new Graph<>(courseItems, edgeItems);
        }
      });
      Gson gson = gsonBuilder.create();
      return gson.fromJson(reader, type);
  }

    public T readGraphFromJson() throws Exception {
      try (Reader reader = new FileReader(jsonFilePath)) {
        return readGraphFromJsonInternal(reader);
      } catch (IOException e) {
        e.printStackTrace();
      }
      throw new Exception();
    }

    public T readGraphFromJsonString() throws Exception {
      try (Reader reader = new StringReader(jsonString)) {
        return readGraphFromJsonInternal(reader);
      } catch (IOException e) {
        e.printStackTrace();
      }
      throw new Exception();
    }
  }

  public JsonResourceManagerAction with(final String jsonFilePath, final Type type, final Type nodeType) {
    return new JsonResourceManagerAction(type, nodeType, jsonFilePath, null);
  }

  public JsonResourceManagerAction withJsonString(final String jsonString, final Type type, final Type nodeType) {
    return new JsonResourceManagerAction(type, nodeType, null, jsonString);
  }
}
