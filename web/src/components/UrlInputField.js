/**
 * UrlInputField.js
 *
 * This is a UrlInputField component in the UI
 *
 */
import React from "react";

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: props.defaultUrl
    };
  }
  handleUrlChange(e) {
    this.setState({ url: e.target.value }, () =>
      this.props.handleUrlChange(this.state.url)
    );
  }

  urlCorrect(url) {
    return url && url.indexOf("http") >= 0;
  }
  render() {
    return (
      <div>
        <input
          className="form-control"
          placeholder="https://..."
          value={this.state.url}
          onChange={e => this.handleUrlChange(e)}
        />
        <br />
        <button
          type="button"
          className="btn btn-primary btn-lg btn-block"
          onClick={e => this.props.handleSubmit(e)}
          disabled={
            !this.urlCorrect(this.state.url) || this.props.processing
          }
        >
          {this.props.buttonText}
        </button>
      </div>
    );
  }
}
