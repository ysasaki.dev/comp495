/**
 * EditTopicModelPage.js
 * 
 * This is a UI component for Edit topic model page
 * 
 */
import React from 'react';
import SortableTree, { addNodeUnderParent, removeNodeAtPath } from 'react-sortable-tree';
import 'react-sortable-tree/style.css';
import Modal from 'react-bootstrap-modal';
import 'react-bootstrap-modal/lib/css/rbm-patch.css';

class ListIterator {

  constructor(elements) {
    this.elements = elements;
  }

  hasNext() {
    return this.elements.length > 0;
  }

  next() {
    return this.elements.shift();
  }

}

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      processing: false,
      buttonText: 'Save changes',
      modal: {
        title: '',
        body: '',
        action: '', // delete, add
        path: '',
        open: false,
        selectedOption: ''
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ data: nextProps.data });
  }

  getAllAdjacentNodesIterator(node, data) {
    const nodeIds = data.links.filter((edge) => {
      return edge.target === node.id;
    }).map((edge) => {
      return edge.source;
    });
    const adjacenetNodes = data.nodes.filter((node) => {
      return nodeIds.indexOf(node.id) >= 0;
    });
    return new ListIterator(adjacenetNodes);
  }

  getNodeKey({ treeIndex }) {
    return treeIndex;
  }

  handleChange(e) {
    this.setState({
      modal: Object.assign({}, this.state.modal, {
        selectedOption: e.target.value
      })
    });
  }

  handleSubmit() {
    this.props.handleSave(this.state.data);
  }

  handleReset() {
    this.props.handleReset();
  }

  handleAddChildNodeClick(node, path) {

    const $options = this.props.topics.map((topic) => {
      return (<option key={topic} value={topic}>{topic}</option>);
    });

    this.setState({
      modal: {
        selectedOption: this.props.topics[0],
        title: 'Add a new topic',
        body: <select className="form-control" value={this.state.modal.selectedOption} onChange={(e) => this.handleChange(e)}>{$options}</select>,
        action: 'add',
        path: path,
        open: true
      }
    });

  }

  handleRemoveNodeClick(node, path) {

    this.setState({
      modal: {
        title: 'Confirm action',
        body: 'You are about to remove the node "' + node.title + '"',
        action: 'delete',
        path: path,
        open: true,
        selectedOption: node.title
      }
    });
  }

  handleModalPrimaryClick() {

    if (this.state.modal.action === 'delete') {
      this.setState((state) => ({
        data: removeNodeAtPath({
          treeData: state.data,
          path: this.state.modal.path,
          getNodeKey: this.getNodeKey
        })
      }), this.handleSubmit);
    } else if (this.state.modal.action === 'add') {
      this.setState((state) => ({
        data: addNodeUnderParent({
          treeData: state.data,
          parentKey: this.state.modal.path[this.state.modal.path.length - 1],
          expandParent: true,
          getNodeKey: this.getNodeKey,
          newNode: {
            title: this.state.modal.selectedOption
          }
        }).treeData
      }), this.handleSubmit);
    }

    this.handleModalCancel();
  }

  handleModalCancel() {
    this.setState({
      modal: {
        title: '',
        body: '',
        action: '',
        path: '',
        open: false,
        selectedOption: ''
      }
    });
  }

  render() {

    return (
      <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 className="page-header">
          Edit topic model <button type="button" className="btn btn-primary" onClick={(e) => this.handleReset(e)} >Reset</button>
        </h1>
        <div style={{ height: window.innerHeight }}>
          {
            this.state.data &&
            <SortableTree
              treeData={this.state.data}
              onChange={(treeData) => this.setState({ data: treeData })}
              generateNodeProps={({ node, path }) => ({
                buttons: [
                  <a key={node.title + '-add'} className='btn btn-sm' onClick={() => this.handleAddChildNodeClick(node, path)} >
                    <i className='fas fa-plus'></i>
                  </a>,
                  <a key={node.title + '-remove'} className='btn btn-sm' onClick={() => this.handleRemoveNodeClick(node, path)} >
                    <i className='fas fa-trash-alt'></i>
                  </a>
                ]
              })}
            />
          }
          {
            !this.state.data &&
            <h2>Loading...</h2>
          }
        </div>
        <Modal
          show={this.state.modal.open}
          onHide={() => this.handleModalCancel()}
          aria-labelledby="ModalHeader">
          <Modal.Header closeButton>
            <Modal.Title id='ModalHeader'>{this.state.modal.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>{this.state.modal.body}</p>
          </Modal.Body>
          <Modal.Footer>
            <Modal.Dismiss className='btn btn-default'>Cancel</Modal.Dismiss>
            <button className='btn btn-primary' onClick={() => this.handleModalPrimaryClick()}>
              Confirm
            </button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

}
