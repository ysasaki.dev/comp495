/**
 * CheckCoursePrerequisitesPage.js
 *
 * This is a UI component for Check course prerequisite page
 *
 */
import React from "react";
import Store from "../store";
import UrlInputField from "./UrlInputField";
const store = new Store();

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      processing: false,
      buttonText: "Check",
      hasError: false,
      result: undefined,
      queue: undefined,
      url:
        "http://www.athabascau.ca/course/course-listings.php?/undergraduate/all/all"
    };
  }

  componentDidMount() {
    setInterval(() => {
      store.queue(queue => {
        this.setState({
          queue: queue
        });
      });
    }, 1000);
  }

  handleSubmit() {
    this.setState({
      processing: true,
      buttonText: "Processing...Please wait",
      result: undefined
    });
    store.buildKnowledgeGraph(this.state.url, (response, hasError) => {
      if (hasError) {
        this.setState({
          processing: false,
          buttonText: "Check",
          hasError: true,
          result: <span>Error occurred! {JSON.stringify(response)}</span>
        });
        return;
      }

      if (!response) {
        this.setState({
          processing: false,
          buttonText: "Check",
          hasError: true,
          result: (
            <span>
              There are pending jobs. Please wait for them to finish executing
              first.
            </span>
          )
        });
        return;
      }

      setTimeout(() => {
        store.queue(queue => {
          this.setState({
            queue: queue
          });
        });
        this.setState({
          processing: false,
          buttonText: "Check",
          hasError: false,
          result: response
        });
      }, 500);
    });
  }

  handleUrlChange(url) {
    this.setState({
      url: url
    });
  }

  render() {
    return (
      <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 className="page-header">Build Knowledge Base</h1>
        <p>Enter the URL of a program curriculum page:</p>
        <UrlInputField
          defaultUrl={this.state.url}
          handleUrlChange={e => this.handleUrlChange(e)}
          handleSubmit={e => this.handleSubmit(e)}
          buttonText={this.state.buttonText}
          processing={this.state.processing}
        />
        {this.state.processing && (
          <div>
            <br />
            <i className="fas fa-spinner fa-pulse fa-7x" />
            <br />
          </div>
        )}
        {!this.state.queue && (
          <div>
            <br />
            <i className="fas fa-spinner fa-pulse fa-7x" /> Checking Job
            queue...
            <br />
          </div>
        )}
        {this.state.hasError && (
          <h1>
            <small>{this.state.result}</small>
          </h1>
        )}
        {this.state.queue && (
          <React.Fragment>
            <hr />
            <h2>Job queue</h2>
            <h3>
              <small>
                Please note that each job could take over 5 - 10 minutes to
                execute.
              </small>
            </h3>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Job ID</th>
                  <th />
                  <th>Job Details</th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(this.state.queue)
                  .reverse()
                  .map(id => (
                    <tr>
                      <td style={{ width: "10%" }}>{id}</td>
                      <td style={{ textAlign: "right", width: "10%" }}>
                        {this.state.queue[id][1] === "Successful" ? (
                          <i class="fas fa-check" />
                        ) : (
                          <i class="far fa-clock" />
                        )}
                      </td>
                      <td style={{ width: "80%" }}>
                        <dl>
                          <dt>Start</dt>
                          <dd>{this.state.queue[id][1]}</dd>
                          {this.state.queue[id][3] && (
                            <React.Fragment>
                              <dt>End</dt>
                              <dd>{this.state.queue[id][3]}</dd>
                            </React.Fragment>
                          )}
                          <dt>Status</dt>
                          <dd>
                            {this.state.queue[id][2] === "Failed"
                              ? this.state.queue[id][2] + ": " + this.state.queue[id][4]
                              : this.state.queue[id][2]}
                          </dd>
                        </dl>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </React.Fragment>
        )}
      </div>
    );
  }
}
