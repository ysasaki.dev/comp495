/**
 * Navigation.js
 * 
 * This is a Navigation component in the UI
 * 
 */
import React from 'react';
import { Link } from 'react-router-dom';
import '../index.css';

class Navigation extends React.Component {

  render() {
    return (
      <nav className="navbar navbar-inverse navbar-fixed-top">
        <div className="container-fluid">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span className="sr-only">Toggle navigation</span>
            </button>
            <Link to="/" className="navbar-brand">{this.props.title}</Link>
          </div>
          <div id="navbar" className="navbar-collapse collapse">
          </div>
        </div>
      </nav>
    );
  }
}

export default Navigation;