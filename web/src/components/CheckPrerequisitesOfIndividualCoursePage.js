/**
 * CheckCourseCurriculumPage.js
 *
 * This is a UI component for Check course curriculum page
 *
 */
import React from "react";
import Store from "../store";
const store = new Store();

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCourseTitle: undefined,
      processing: false,
      buttonText: "Check",
      result: undefined
    };
  }

  componentDidMount() {
    const self = this;
    const radix = 10;
    store.getUniversityCourses(data => {
      data = data.sort((a, b) => {
        const aTitle = parseInt(
          new RegExp("([0-9].*)").exec(a.title)[0],
          radix
        );
        const bTitle = parseInt(
          new RegExp("([0-9].*)").exec(b.title)[0],
          radix
        );
        return aTitle > bTitle ? 1 : aTitle < bTitle ? -1 : 0;
      });
      self.setState({
        courses: data,
        selectedCourseTitle: data[0].title
      });
    });
  }

  handleChange(event) {
    this.setState({
      selectedCourseTitle: event.target.value,
      result: undefined
    });
  }

  handleSubmit() {
    this.setState({
      processing: true,
      buttonText: "Processing...Please wait"
    });
    store.checkPrerequisitesOfIndividualCoursePage(
      this.props.data,
      this.state.selectedCourseTitle,
      response => {
        setTimeout(() => {
          this.setState({
            processing: false,
            buttonText: "Check",
            result: response
          });
        }, 500);
      }
    );
  }

  render() {
    return (
      <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 className="page-header">
          Check Prerequisites Of Individual Course Page
        </h1>
        <p>Please choose a course</p>
        <select
          className="form-control input-lg"
          onChange={e => this.handleChange(e)}
        >
          {this.state.courses &&
            this.state.courses.map((course, index) => (
              <option key={index} value={course.title}>
                {course.title}
              </option>
            ))}
        </select>
        <br />
        {this.state.selectedCourseTitle && (
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">{this.state.selectedCourseTitle}</h3>
            </div>
            <div className="panel-body">
              {
                this.state.courses.filter(
                  c => c.title === this.state.selectedCourseTitle
                )[0].description
              }
              <hr />
              {
                this.state.courses.filter(
                  c => c.title === this.state.selectedCourseTitle
                )[0].prerequisites
              }
            </div>
          </div>
        )}
        <button
          type="button"
          className="btn btn-primary btn-lg btn-block"
          onClick={e => this.handleSubmit(e)}
          disabled={this.state.processing}
        >
          {this.state.buttonText}
        </button>
        {this.state.processing && (
          <div>
            <br />
            <i className="fas fa-spinner fa-pulse fa-7x" />
            <br />
          </div>
        )}
        {this.state.result &&
          this.state.result.courseDetails.length === 0 && (
            <div>
              <h2>Result</h2>
              Unable to check course curriculum. Possible reasons:
              <ul>
                <li>
                  There are no prerequisites set for this course in the current
                  curriculum.
                </li>
                <li>
                  The knowledge base could not extract prerequisit information
                  based on the supplied course description and prerequisite
                  description.
                </li>
                <li>
                  Topic model may not be covering the dependent topics for the
                  course.
                </li>
              </ul>
            </div>
          )}
        {this.state.result &&
          this.state.result.courseDetails.length > 0 && (
            <div>
              <h2>Result</h2>
              {this.state.result.topicCoveredDetails &&
                this.state.result.topicCoveredDetails.length === 0 && (
                  <div>
                    Unable to determine the dependent topics for this course.
                    Possible reasons:
                    <ul>
                      <li>
                        The topic dependencies need to be properly set for each
                        topic of the course.
                      </li>
                    </ul>
                  </div>
                )}
              {this.state.result.topicCoveredDetails &&
                this.state.result.topicCoveredDetails.length > 0 && (
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th style={{ textAlign: "right" }}>Cocvered?</th>
                        <th>Topics to be covered before taking this course</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.result.topicCoveredDetails.map(
                        topicCoveredDetails => (
                          <tr key={"covered-" + topicCoveredDetails.topic}>
                            <td style={{ textAlign: "right" }}>
                              {topicCoveredDetails.covered ? (
                                <i className="fas fa-check-circle" />
                              ) : (
                                <span>-</span>
                              )}
                            </td>
                            <td>{topicCoveredDetails.topic}</td>
                          </tr>
                        )
                      )}
                    </tbody>
                  </table>
                )}
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Chain of Courses</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.result.courseDetails.map(courseDetails => (
                    <tr key={"chain-" + courseDetails.courseItem.id}>
                      <td>
                        {courseDetails.courseItem.id} - currently required by{" "}
                        {courseDetails.dependentFor.id}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <h2>Details</h2>
              {this.state.result.courseDetails.map(courseDetails => (
                <div key={"details-" + courseDetails.courseItem.id}>
                  <h3>{courseDetails.courseItem.id}</h3>
                  <h4>
                    Prerequisites by{" "}
                    <span className="label label-info">
                      {courseDetails.dependentFor.id}
                    </span>
                  </h4>
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th style={{ textAlign: "right" }}>Cocvered?</th>
                        <th>Topics</th>
                      </tr>
                    </thead>
                    <tbody>
                      {courseDetails.courseItem.impliedTopics.map(
                        impliedTopic => (
                          <tr
                            key={
                              "topics-" +
                              courseDetails.courseItem.id +
                              " " +
                              impliedTopic
                            }
                          >
                            <td style={{ width: "50%", textAlign: "right" }}>
                              {courseDetails.coveredTopics.indexOf(
                                impliedTopic
                              ) >= 0 ? (
                                <i className="fas fa-check-circle" />
                              ) : (
                                <span>-</span>
                              )}
                            </td>
                            <td>{impliedTopic}</td>
                          </tr>
                        )
                      )}
                    </tbody>
                  </table>
                </div>
              ))}
            </div>
          )}
      </div>
    );
  }
}
