/**
 * Sidebar.js
 * 
 * This is a Sidebar component in the UI
 * 
 */
import React from 'react';
import { Link } from 'react-router-dom';

class Sidebar extends React.Component {

  getModules() {
    const $modules = [];
    this.props.routes.forEach((r) => {

      const className = 'nav-link ' + (r.text === this.props.selectedModule ? 'active' : '');
      $modules.push(
        <li key={r.path}>
          <Link to={r.path} className={className}>{r.text}</Link>
        </li>
      );

    });

    return $modules;
  }

  render() {
    return (
      <div className="col-sm-3 col-md-2 sidebar">
        <ul className="nav nav-sidebar">
          {this.getModules()}
        </ul>
      </div>
    );
  }
}

export default Sidebar;