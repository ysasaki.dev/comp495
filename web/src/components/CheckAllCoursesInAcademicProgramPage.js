/**
 * CheckCoursePrerequisitesPage.js
 *
 * This is a UI component for Check course prerequisite page
 *
 */
import React from "react";
import Store from "../store";
const store = new Store();

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      processing: false,
      buttonText: "Check",
      result: undefined
    };
  }

  handleSubmit() {
    this.setState({
      processing: true,
      buttonText: "Processing...Please wait",
      result: undefined
    });
    store.checkAllCoursesInAcademicProgramPage(this.props.data, response => {
      response.sort((a, b) => {
        return a.selectedCourse.id > b.selectedCourse.id
          ? 1
          : a.selectedCourse.id < b.selectedCourse.id
            ? -1
            : 0;
      });

      setTimeout(() => {
        this.setState({
          processing: false,
          buttonText: "Check",
          result: response
        });
      }, 500);
    });
  }

  render() {
    return (
      <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 className="page-header">Check All Courses In Academic Program</h1>
        <button
          type="button"
          className="btn btn-primary btn-lg btn-block"
          onClick={e => this.handleSubmit(e)}
          disabled={this.state.processing}
        >
          {this.state.buttonText}
        </button>
        {this.state.processing && (
          <div>
            <br />
            <i className="fas fa-spinner fa-pulse fa-7x" />
            <br />
          </div>
        )}
        {this.state.result &&
          this.state.result.length === 0 && (
            <div>
              <h2>No prerequisites are found</h2>
            </div>
          )}
        {this.state.result &&
          this.state.result.length > 0 && (
            <div>
              <h2>Result</h2>
              {this.state.result.map((res, index) => (
                <div key={"table" + index}>
                  <h3>
                    {res.selectedCourse.id}
                    &nbsp;&nbsp;&nbsp;
                    {res.topicDetails &&
                      res.topicDetails.length === 0 && (
                        <span className="label label-success">OK</span>
                      )}
                    {res.topicDetails &&
                      res.topicDetails.length > 0 && (
                        <span className="label label-danger">
                          Missing prerequisites
                        </span>
                      )}
                  </h3>
                  {res.topicDetails &&
                    res.topicDetails.length > 0 && (
                      <table className="table table-striped">
                        <thead>
                          <tr>
                            <th>Course topics</th>
                            <th>Covered by Curriculum?</th>
                            <th>Recommended Prerequisites</th>
                          </tr>
                        </thead>
                        <tbody>
                          {res.topicDetails.map(topicDetails => (
                            <tr key={topicDetails.topic}>
                              <td>{topicDetails.topic}</td>
                              <td>{topicDetails.exists ? "Yes" : "No"}</td>
                              <td>
                                {
                                  <ul>
                                    {topicDetails.recommendedCourses.map(
                                      recCourse => (
                                        <li key={recCourse.id}>
                                          {recCourse.id}
                                        </li>
                                      )
                                    )}
                                    {topicDetails.recommendedCourses.length ===
                                      0 && (
                                      <p>
                                        There are no courses that offer this
                                        topic
                                      </p>
                                    )}
                                  </ul>
                                }
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    )}
                </div>
              ))}
            </div>
          )}
      </div>
    );
  }
}
