/**
 * Store.js
 *
 * This class provides access to the backend REST endpoint
 *
 */
import axios from "axios";

class Store {
  constructor() {
    this.instance = axios.create({
      // baseURL: "http://localhost:4567"
      baseURL: 'http://138.197.141.38:4567'
    });
  }

  send(promise, callback) {
    return promise
      .then(res => {
        callback(res.data, false);
      })
      .catch(err => {
        console.error(err);
        callback(err, true);
        return err;
      });
  }

  getUniversityCourses(callback) {
    return this.send(this.instance.get("/courses"), callback);
  }

  getTopicModel(callback) {
    return this.send(this.instance.get("/topicModels"), callback);
  }

  queue(callback) {
    return this.send(this.instance.get("/queue"), callback);
  }

  buildKnowledgeGraph(url, callback) {
    const data = {
      input: url,
      discipline: "comp"
    };
    return this.send(
      this.instance.post("/buildKnowledgeGraph", data),
      callback
    );
  }

  checkAllCoursesInAcademicProgramPage(topicModel, callback) {
    const data = {
      topicModel: topicModel
    };
    return this.send(
      this.instance.post("/checkAllCoursesInAcademicProgramPage", data),
      callback
    );
  }

  checkPrerequisitesOfIndividualCoursePage(topicModel, courseTitle, callback) {
    const data = {
      topicModel: topicModel,
      input: courseTitle
    };
    return this.send(
      this.instance.post("/checkPrerequisitesOfIndividualCoursePage", data),
      callback
    );
  }
}

export default Store;
