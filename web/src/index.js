/**
 * index.js
 *
 * This is an entry point for the React App.
 *
 */
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navigation from "./components/Navigation";
import Sidebar from "./components/Sidebar";
import EditTopicModelPage from "./components/EditTopicModelPage";
import BuildKnowledgeBasePage from "./components/BuildKnowledgeBasePage";
import CheckAllCoursesInAcademicProgramPage from "./components/CheckAllCoursesInAcademicProgramPage";
import CheckPrerequisitesOfIndividualCoursePage from "./components/CheckPrerequisitesOfIndividualCoursePage";
import Store from "./store";
import TreeService from "./treeService";
import "./index.css";

const store = new Store();
const treeService = new TreeService();

class Root extends React.Component {
  routes = [
    {
      path: "/",
      text: "1. Build Knowledge Base",
      exact: true,
      main: () => (
        <BuildKnowledgeBasePage
          data={this.state.editData}
          courses={this.state.courses}
        />
      )
    },
    {
      path: "/edittopicmodel",
      text: "2. Edit topic model",
      exact: true,
      main: () => (
        <EditTopicModelPage
          data={this.state.data}
          topics={this.state.topics}
          handleSave={data => this.handleSave(data)}
          handleReset={() => this.handleReset()}
        />
      )
    },
    {
      path: "/curriculum",
      text: "3. Check all courses in an academic program",
      exact: true,
      main: () => (
        <CheckAllCoursesInAcademicProgramPage
          data={this.state.editData}
          courses={this.state.courses}
        />
      )
    },
    {
      path: "/prerequisites",
      text: "4. Check prerequisite of an individual course",
      exact: true,
      main: () => (
        <CheckPrerequisitesOfIndividualCoursePage
          data={this.state.editData}
          courses={this.state.courses}
        />
      )
    }
  ];

  constructor(props) {
    super(props);

    this.state = {
      site: {
        title: "COMP495 Depedency Checker"
      },
      selectedModule: "Edit topic model",
      modules: {
        courseCheck: {
          selectedCourse: undefined
        },
        curriculumCheck: {}
      },
      originalData: undefined,
      editData: undefined,
      allData: {
        nodes: [],
        edges: []
      },
      data: undefined,
      topics: [],
      courses: []
    };

    store.getUniversityCourses(data => {
      this.setState({ courses: data });
    });
  }

  applyFilter(oldData) {
    // copy
    const data = JSON.parse(JSON.stringify(oldData));

    let nodes = data.nodes;
    let links = data.links;

    if (
      this.state.selectedCourseName &&
      data.nodes.find(n => n.id === this.state.selectedCourseName)
    ) {
      const selectedNodes = [this.state.selectedCourseName];

      links = links.filter(e => {
        return (
          selectedNodes.indexOf(e.source) >= 0 ||
          selectedNodes.indexOf(e.target) >= 0
        );
      });

      nodes = nodes.filter(n => {
        return links.find(l => l.source === n.id || l.target === n.id);
      });

      if (nodes.length === 0) {
        nodes = nodes.filter(function(n) {
          return n.id === this.state.selectedCourseName;
        });
      }
    }

    return {
      nodes: nodes,
      links: links
    };
  }

  setTopicModelData() {
    store.getTopicModel(data => {
      const allData = treeService.convertToGraphData(data);
      this.setState({
        originalData: data,
        editData: data,
        allData: allData,
        data: treeService.convertToTree(allData),
        topics: treeService.createCourseFilterData(data)
      });
    });
  }

  componentDidMount() {
    this.setTopicModelData();
  }

  handleSelectModule(selectedModule) {
    this.setState({
      selectedModule: selectedModule
    });
  }

  handleSelectCourse(selected) {
    this.setState(
      {
        selectedCourseName: selected[0]
      },
      () => {
        this.setState({
          data: this.convertToTree(this.applyFilter(this.state.allData))
        });
      }
    );
  }

  handleSave(tree) {
    this.setState({
      data: tree,
      editData: treeService.convertTreeToGraph(tree)
    });
  }

  handleReset() {
    const allData = treeService.convertToGraphData(this.state.originalData);
    this.setState({
      editData: this.state.originalData,
      allData: allData,
      data: treeService.convertToTree(allData),
      topics: treeService.createCourseFilterData(this.state.originalData)
    });
  }

  render() {
    return (
      <Router>
        <div>
          <Navigation
            title={this.state.site.title}
            handleSelectCourse={selected => this.handleSelectCourse(selected)}
          />

          <div className="container-fluid">
            <div className="row">
              <Sidebar
                routes={this.routes}
                universities={this.state.universities}
                selectedUniversityId={this.state.selectedUniversityId}
                handleSelectModule={selected =>
                  this.handleSelectModule(selected)
                }
              />
            </div>
            {this.routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </div>
        </div>
      </Router>
    );
  }
}

// ========================================

ReactDOM.render(<Root />, document.getElementById("root"));
