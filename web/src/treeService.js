/**
 * TreeService.js
 *
 * This class provides utility operations for grpah / tree
 *
 */
class TreeService {
  findIndex(collection, callback) {
    let index = -1;
    collection.forEach((el, i) => {
      if (callback(el)) {
        index = i;
      }
    });
    return index;
  }

  convertToGraphData(rawData) {
    const nodes = rawData.nodes.map(n => {
      return {
        id: n.id
      };
    });

    const links = rawData.edges.map(e => {
      return {
        source: e.source.id,
        target: e.target.id
      };
    });

    return {
      nodes: nodes,
      links: links
    };
  }

  buildTreeNode(id) {
    return {
      title: id,
      children: [],
      expanded: true
    };
  }

  getAllAdjacentNodes(node, data) {
    const nodeIds = data.links
      .filter(edge => {
        return edge.target === node.id;
      })
      .map(edge => {
        return edge.source;
      });
    const adjacenetNodes = data.nodes.filter(node => {
      return nodeIds.indexOf(node.id) >= 0;
    });
    return adjacenetNodes;
  }

  bfs(node, data) {
    const tree = this.buildTreeNode(node.id);
    const adjacentNodes = this.getAllAdjacentNodes(node, data);

    adjacentNodes.forEach(childNode => {
      const subTree = this.bfs(childNode, data);
      tree.children.push(subTree);
    });
    return tree;
  }

  getLongestTreeHeight(tree, currentHeight) {
    if (tree.children.length === 0) {
      return currentHeight;
    }

    let height = currentHeight;
    tree.children.forEach(tree => {
      const theHeight = this.getLongestTreeHeight(tree, currentHeight + 1);
      if (theHeight > height) {
        height = theHeight;
      }
    });

    return height;
  }

  treeContainsSubTree(tree, subTree) {
    let found = false;
    tree.children.forEach(theSubTree => {
      if (theSubTree.title === subTree.title) {
        found = true;
      } else if (!found) {
        found = this.treeContainsSubTree(theSubTree, subTree);
      }
    });
    return found;
  }

  removeDuplicateSubTrees(parentTree) {
    // find the longest sub-tree
    let bestTree = this.buildTreeNode();
    let height = 0;
    parentTree.children.forEach(subTree => {
      const theHeight = this.getLongestTreeHeight(subTree, 0);
      if (theHeight > height) {
        height = theHeight;
        bestTree = subTree;
      }
    });

    // get other sub-trees if longest tree contains it
    const siblingTrees = parentTree.children.filter(
      subTree => subTree !== bestTree
    );

    // get sub-trees which are contained in the chain of the longest sub-tree.
    const siblingsToBeRemoved = [];
    siblingTrees.forEach(siblingTree => {
      if (this.treeContainsSubTree(bestTree, siblingTree)) {
        siblingsToBeRemoved.push(siblingTree);
      }
    });

    // remove the siblings
    siblingsToBeRemoved.forEach(siblingTree => {
      const matchSiblingTree = parentTree.children.filter(
        c => c.title === siblingTree.title
      )[0];
      const index = parentTree.children.indexOf(matchSiblingTree);
      if (index > -1) {
        parentTree.children.splice(index, 1);
      }
    });

    // for each sibling, remove duplicate.
    parentTree.children.forEach(siblingTree => {
      this.removeDuplicateSubTrees(siblingTree);
    });
  }

  createCourseFilterData(data) {
    return data.nodes.map(n => {
      return n.id;
    });
  }

  convertToTree(data) {
    const rootTree = this.buildTreeNode("root");

    // make a tree
    data.nodes.forEach(node => {
      const subTree = this.bfs(node, data);
      rootTree.children.push(subTree);
    });

    this.removeDuplicateSubTrees(rootTree);

    return rootTree.children;
  }

  buildGraphNodeFromTreeNode(treeNode) {
    return {
      id: treeNode.title,
      label: treeNode.title
    };
  }

  buildGraphEdgeFromNodes(source, target) {
    return {
      id: source.id + "=>" + target.id,
      source: source,
      target: target
    };
  }

  bfsT2G(treeParent, treeChildren, nodes, edges) {
    if (treeChildren) {
      treeChildren.forEach(treeChildNode => {
        const graphNode = this.buildGraphNodeFromTreeNode(treeChildNode);
        if (nodes.filter(n => n.id === graphNode.id).length === 0) {
          nodes.push(graphNode);
        }

        if (treeParent) {
          const source = graphNode;
          const target = nodes.filter(a => a.id === treeParent.title)[0];
          if (
            edges.filter(e => e.source === source && e.target === target)
              .length === 0
          ) {
            const edge = this.buildGraphEdgeFromNodes(source, target);
            edges.push(edge);
          }
        }

        // bfs
        this.bfsT2G(treeChildNode, treeChildNode.children, nodes, edges);
      });
    }
  }

  convertTreeToGraph(tree) {
    const nodes = [];
    const edges = [];
    this.bfsT2G(undefined, tree, nodes, edges);
    const graph = {
      nodes: nodes,
      edges: edges
    };
    return graph;
  }
}

export default TreeService;
